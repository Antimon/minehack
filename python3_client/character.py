

import time
import math

class character:



    def __init__(self):
        self.Name = "HansPeter"
        self.PosY=0
        self.PosX=0
        self.PosZ=7
        self.SightRadius = 0
        self.Energy = 100
        self.Speed = 5
        self.LowestZinSightField = 0
        self.CharactersThisCharacterCanSee = []         #this will be a array with tupels of: the name of the foreign character and his position
        print("character created")



    def updateSightField(self, In_NewSightField):
        #self.debug("character.updateSightField before replacing:")
        #self.debug(In_NewSightField)
        self.SightField = {}            #erase old sightField
        self.SightField = In_NewSightField
        #self.debug("character.updateSightField after replacing:")
        #self.debug(self.SightField)
        

    def debug(self, In):
        Stream = open("log/debug.log", "a")
        Stream.write("[" + str(time.time()) + "]: " + str(In)+"\n")
        Stream.close()


    def handleNewCharacter(self, In_CharacterCube):
        self.debug("enter handleNewCharacter()")
        self.CharactersThisCharacterCanSee.append(In_CharacterCube)
        
        
        
    def cleanUpList_CharactersThisCharacterCanSee(self):
        Index = 0
        for Cube in self.CharactersThisCharacterCanSee:
            Distance = math.sqrt( (Cube.PosY - self.PosY)**2 + (Cube.PosX - self.PosX)**2 + (Cube.PosZ - self.PosZ)**2 )
            if Distance > self.SightRadius:
                self.CharactersThisCharacterCanSee.pop(Index)
                self.debug( "deleted element from list with index : " + str( Index ) )
            Index += 1
        
        

#    def CalculateListWithCharactersSortedByDistance(self):                #gets called after updatePosition(). that way i already have the new position and can sort the the list by distance
#        self.ListWithCharactersSeenByMe_sortedByDistance.clear()
#        DistanceSquared = 0
#        for OtherCharactercube in self.ListWithCharactersSeenByMe:
#            DistanceSquared = ( int(self.PosY) - int(OtherCharactercube.PosY))*( int(self.PosY) - int(OtherCharactercube.PosY) ) + ( int(self.PosX) - int(OtherCharactercube.PosX) )*( int(self.PosX) - int(OtherCharactercube.PosX) ) + ( int(self.PosZ) - int(OtherCharactercube.PosZ) )*( int(self.PosZ) - int(OtherCharactercube.PosZ) )
#            Temp_Tupel = ( DistanceSquared, OtherCharactercube )
#            self.ListWithCharactersSeenByMe_sortedByDistance.append( Temp_Tupel )    #still unsorted yet
#    
#            #PositionInList = PositionInList + 1
#        self.ListWithCharactersSeenByMe_sortedByDistance = sorted( self.ListWithCharactersSeenByMe_sortedByDistance, key = lambda x: x[0] )
#        
#        PositionInList = 0
#        self.PositionInListThisCharacterIsTargetting = -1
#        for OtherCharactercube in self.ListWithCharactersSeenByMe_sortedByDistance:
#            if OtherCharactercube[1].AccountID == self.Target:
#                self.PositionInListThisCharacterIsTargetting = PositionInList
#            PositionInList = PositionInList + 1
        
