#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>     //usleep
#include <sys/types.h>      //
#include <sys/socket.h>     //        = needed for the socket and server stuff
#include <netinet/in.h>     //
//#include <time.h>
#include <thread>
#include <mutex>
#include <fstream>
#include <sys/timeb.h>          //needed for timestap in log-file
#include <boost/algorithm/string.hpp>       //needed for splitting the received Messages in interpretReceivedMessage()
#include <algorithm>                   //needed for max() for calculating sight field
#include <sys/time.h>           //needed for timestamp
//#include <ctime>                   //needed for timestamp
#include <set>
#include <sys/stat.h>       //filesystem operations


#include "controller.h"


using namespace std;






void error(const char *msg){			//This function is called when a system call fails. It displays a message about the error on stderr and then aborts the program. The perror man page gives more information.
    perror(msg);
    string strExit;
    cout << "press any key to exit" << endl;
    cin >> strExit;

    exit(1);
}

controller::controller( parameters In_Parameters ){

    Parameters = In_Parameters;

    world Temp_World(&Database);
    this->World = Temp_World;

    if( Parameters.MakeNew ){
        World.createNew(Parameters.MakeFlatWorld);
        World.writeWorldInDatabase();
    }else{
        World.loadWorldFromDatabase();
    }

    ServerPassword = Parameters.Password;

    PortNumber = PORT_NUMBER;
    ServerIsRunning = true;
    IdentifierForClients = 0;

    //!AllClients = new client* [NUMBER_OF_CLIENTS];

    for ( int Temp_SightRadius = 0; Temp_SightRadius <= MAX_SIGHT_RADIUS; Temp_SightRadius++){
        SightLibrary.calculateSightFieldMapWithRadius(Temp_SightRadius);
    }


    if( Parameters.MakeNew ){       //safing the new generated world
        //!Database.writeWorld( &World );
    }

}


controller::~controller(){

    /*!
    for (uint8_t i = 0; i < 42; i++){
        delete AllClients[i];
    }
    delete[] AllClients;
    */

}


void controller::initYourself(){


    cout << getTimestampInMiliseconds() << " listen on Port " << PORT_NUMBER << endl;


    SocketFileDescriptor = socket(AF_INET, SOCK_STREAM, 0);          //creates a new socket. It takes three arguments.
                                                        //The first is the address domain of the socket. for more see http://www.linuxhowtos.org/C_C++/socket.htm
                                                        //The second argument is the type of socket.
                                                        //The third argument is the protocol. If this argument is zero (and it always should be except for unusual circumstances), the operating system will choose the most appropriate protocol. It will choose TCP for stream sockets and UDP for datagram sockets.
                                                        //The socket system call returns an entry into the file descriptor table (i.e. a small integer). This value is used for all subsequent references to this socket. If the socket call fails, it returns -1.
    if (SocketFileDescriptor < 0){
        error("ERROR opening socket");
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));     //sets all values in a buffer to zero. It takes two arguments, the first is a pointer to the buffer and the second is the size of the buffer. Thus, this line initializes serv_addr to zeros.

    //we dont need the following because we use scanf to get the portnumber
  //  portNumber = atoi(argv[1]);                            //The port number on which the server will listen for connections is passed in as an argument, and this statement uses the atoi() function to convert this from a string of digits to an integer.

    serv_addr.sin_family = AF_INET;                    //define the domain used. we use internet
                                                        //The variable serv_addr is a structure of type struct sockaddr_in. This structure has four fields. The first field is short sin_family, which contains a code for the address family. It should always be set to the symbolic constant AF_INET.

    serv_addr.sin_addr.s_addr = INADDR_ANY;            //Permit any incoming IP adress by declare INADDR_ANY
                                                       //structure of type struct in_addr which contains only a single field unsigned long s_addr. This field contains the IP address of the host. For server code, this will always be the IP address of the machine on which the server is running, and there is a symbolic constant INADDR_ANY which gets this address.

    serv_addr.sin_port = htons(PortNumber);                //unsigned short sin_port, which contain the port number. However, instead of simply copying the port number to this field, it is necessary to convert this to network byte order using the function htons() which converts a port number in host byte order to a port number in network byte order.



    // kill "Address already in use" error message          //http://www.unix.com/programming/29475-how-solve-error-bind-address-already-use.html
    int64_t tr=1;
    if (setsockopt(SocketFileDescriptor,SOL_SOCKET,SO_REUSEADDR,&tr,sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }



    if (bind(SocketFileDescriptor, (struct sockaddr *) &serv_addr,   //The bind() system call binds a socket to an address, in this case the address of the current host and port number on which the server will run. It takes three arguments, the socket file descriptor, the address to which is bound, and the size of the address to which it is bound. The second argument is a pointer to a structure of type sockaddr, but what is passed in is a structure of type sockaddr_in, and so this must be cast to the correct type. This can fail for a number of reasons, the most obvious being that this socket is already in use on this machine. The bind() manual has more information.
            sizeof(serv_addr)) < 0){                   //
            error("ERROR on binding");                 //
    }

    listen(SocketFileDescriptor,5);                     //allows the process to listen on the socket for
                                                        //connections. The first argument is the socket file
                                                        //descriptor, and the second is the size of the
                                                        //backlog queue, i.e., the number of connections that
                                                        //can be waiting while the process is handling a
                                                        //particular connection. This should be set to 5, the
                                                        //maximum size permitted by most systems. If the first
                                                        //argument is a valid socket, this call cannot fail,
                                                        //and so the code doesn't check for errors.
                                                        //The listen() man page has more information.

    SizeOfAdressOfClient = sizeof(cli_addr);             // stores the size of the address of the client.
                                            //This is needed for the accept system call.




}

void controller::waitForMoreClients(){ //called from startThreadForCatchingClients()


    //   vector<client> AllClients;      is now a member-attribute of controller
  //  while(ServerIsRunning){

    usleep(20000);              //! I just did that, because without it messes my terminal-output up because two threads are writing at the same time
                                //! It can be removed later!

    cout << getTimestampInMiliseconds() << " waiting for new client to connect" << endl;

    newSocketFileDescriptor = accept(      //The accept() system call causes the process to block until a client connects to the server.
                                            //Thus, it wakes up the process when a connection from a client has been successfully
                                            //established. It returns a new file descriptor, and all communication on this connection
                                            //should be done using the new file descriptor.
                                        SocketFileDescriptor,
                                        (struct sockaddr *) &cli_addr,      //pointer to the address of the client on the other end of the connection, (must be cast to the correct type)
                                        &SizeOfAdressOfClient         //size of this structure. The accept() man page has more information.
                                                                            );

    if (newSocketFileDescriptor < 0){
        cout << getTimestampInMiliseconds() << " controller::waitForMoreClients(): something went veeeerryyy wrong! newSocketFileDescriptor < 0 after accepting!" << endl;
       // error("ERROR on accept");
    }

    //AllClients[IdentifierForClients] = new client(newSocketFileDescriptor, IdentifierForClients);
    AllClients.push_back( new client(newSocketFileDescriptor, IdentifierForClients) );

    //!Michi said, it would be a good idea to start the thread right here

    IdentifierForClients++;



       // thread ThreadForWaintingForMessages( &controller::startThreadForWaitingForMessages, this );
        //this thread starts right after a new client is connected. it takes the last client on the vector
        //(which should be the latest) and starts with that the routine to wait for incomming messages from the client
        //!this turned out to crash the program. don't know why..



}


void controller::closeSocketFileDescriptor(){

    close( SocketFileDescriptor );

}



void controller::interpretReceivedMessage( string In_ReceivedMessage, int64_t In_IDofClient ){

    MutexForInterpretMessage.lock();


    //!Debug
    timeb Tb;
    ftime (&Tb);
    int64_t TimeStamp = Tb.millitm + (Tb.time & 0xfffff) * 1000;
    fstream Fstream;                          //this writes the interpretReceivedMessage.log
    Fstream.open("interpretReceivedMessage.log", ios::out|ios::app );
    Fstream << "[" <<to_string(TimeStamp) << "]: " << In_ReceivedMessage << endl;
    Fstream.close();

    struct_message Message = extractMessage(In_ReceivedMessage);

    if( Message.EndSymbol == "end" || Message.EndSymbol == "end\n" ){
//                                                             ^need that for netcat-experimens


                    string KeystringForChatMessage=                     "#ChatMessage-----";
                    string KeystringForHeartbleedFromClient =           "#HeartbleedFromCl";
                    string KeystringForIncommingSightArray =            "#SightArray------";
                    string KeystringForRequestToMove =                  "#RequestToMove---";
                    string KeystringForCreateNewAccount =               "#CreateNewAccount";
                    string KeystringForLoginRequest =                   "#RequestToLogin--";
                    string KeystringForClientWantsToCloseConnection =   "#CloseConnection-";
                    string KeystringForPrimaryAttack =                  "#PrimaryAttack---";
                    string KeystringForSecondaryAttack =                "#SecondaryAttack-";
                    string KeystringForAdminCommand =                   "#AdminCommand----";
                    string KeystringForRequestForCompleteSightField =   "#FllSghtFldRquest";


                    if( Message.MessageType == KeystringForChatMessage ){
                        receivedChatMessage( Message.MessageContent, In_IDofClient);
                    }else if( Message.MessageType == KeystringForHeartbleedFromClient ){
                        receivedHeartbleedFromClient(In_IDofClient);
                    }else if( Message.MessageType == KeystringForRequestToMove ){
                        receivedRequestToMove( Message.MessageContent, In_IDofClient );
                    }else if(Message.MessageType == KeystringForCreateNewAccount){
                        receivedCreateNewAccount( Message.MessageContent, In_IDofClient );
                    }else if(Message.MessageType == KeystringForLoginRequest){
                        receivedLoginRequest( Message.MessageContent, In_IDofClient );
                    }else if(Message.MessageType == KeystringForClientWantsToCloseConnection){
                        receivedClientWantsToCloseConnection( In_IDofClient );
                    }else if( Message.MessageType == KeystringForPrimaryAttack ){
                        receivedPrimaryAttack( stoi(Message.MessageContent), In_IDofClient );
                        //cout << "furz" << endl;
                    }else if( Message.MessageType == KeystringForSecondaryAttack ){
                        receivedSecondaryAttack( stoi(Message.MessageContent), In_IDofClient);
                    }else if( Message.MessageType == KeystringForAdminCommand ){
                        receivedAdminCommand( Message.MessageContent, In_IDofClient );
                    }else if( Message.MessageType == KeystringForRequestForCompleteSightField){
                        buildSightFieldFromScratchAndUpdatePosition( In_IDofClient );
                    }else{
                        cout << getTimestampInMiliseconds() << " unknown Message: " << In_ReceivedMessage << "  ; with type: " << Message.MessageType << endl;
                    }
    }else{
        cout << getTimestampInMiliseconds() << " controller::interpretReceivedMessage(): endsymbol missing!" << endl;
    }
    MutexForInterpretMessage.unlock();


}

void controller::startThreadForWaitingForMessages(int64_t In_ID_OfToHandleClient){ //call from startThreadForCatchingClients()

    //!Debug
//    uint64_t Debug_ID_OfToHandleClient = In_ID_OfToHandleClient;
    //cout << "'startThreadForWaitingForMessages()' started with In_ID_OfToHanleClient: " << In_ID_OfToHandleClient << endl;


    bool Temp_ClientStillConnected = AllClients[In_ID_OfToHandleClient]->getIsClientStillConnected();


    while(ServerIsRunning && Temp_ClientStillConnected){

        string ReceivedMsg = "empty";

        ReceivedMsg = AllClients[ In_ID_OfToHandleClient ]->waitForMessageFromClient( &AllUserAccounts[ AllClients[In_ID_OfToHandleClient]->getAccountID() ] );



        if( Parameters.Heartbleed ){
            if( AllClients[In_ID_OfToHandleClient]->getHeartbleedIsAlreadyStarted() == false ){
                AllClients[In_ID_OfToHandleClient]->setHeartbleedIsAlreadyStarted();

                thread StartThreadForHeartbleed( &controller::startThreadForHeartbleed, this, IdentifierForClients-1 );

                StartThreadForHeartbleed.detach();

            }
        }


        #if UNLOCKWRITINGMUTEX
            AllClients[In_ID_OfToHandleClient].sendOrderToUnlockMutexForWritingSocket();
        #endif // UNLOCKWRITINGMUTEX

        Temp_ClientStillConnected = AllClients[In_ID_OfToHandleClient]->getIsClientStillConnected();

        if ( Temp_ClientStillConnected ){                 //i need to make that check here once again, because otherwise the Message would get interpreted anyway even if the client is already disconnected
            interpretReceivedMessage( ReceivedMsg, In_ID_OfToHandleClient );
        }

    }




    cout << "controller::startThreadForWaitingForMessages(): leaving." << endl;
}



void controller::startThreadForCatchingClients(){      //called from: controller::manageYourself()

    waitForMoreClients();       //this function blocks until a new client has connected

    //!int64_t Temp_SizeOfVector = AllClients.size();  //dont need that any more here, because i get the Size from the member variable 'IdentifierForConnectedClients'

    thread StartThreadForWaitingMessages ( &controller::startThreadForWaitingForMessages, this, IdentifierForClients-1 );
    //this function will take the last connected client and will make it wait for a message





    cout << "controller::startThreadForCatchingClients(): detachting thread " << endl;
    StartThreadForWaitingMessages.detach();                 //frees the Thread
    cout << "end of controller::startThreadForCatchingClients()" << endl;

}



void controller::manageYourself(){
    #if DISPLAY_WORLD
       // World.displayWorld();
    #endif // DISPLAY_WORLD

    while(ServerIsRunning){
        cout << "controller::manageYourself(): beginning of ServerIsRunning-loop " << endl;
        startThreadForCatchingClients();
                                                //the function will lock the mutex and then will wait for new clients.
                                                //this scope must wait, untill the last started thread has done to
                                                //make a new thread for waiting for the next client

        cout << "controller::manageYourself(): end of ServerIsRunning-loop " << endl;

    }
    cout << getTimestampInMiliseconds() << " end of manageYourself()" << endl;
}

void controller::startThreadForHeartbleed(int64_t In_ID_OfToHandleClient){     //called from startThreadForCatchingClients()



    bool Temp_ClientStillConnected = AllClients[In_ID_OfToHandleClient]->getIsClientStillConnected();

    while(ServerIsRunning && Temp_ClientStillConnected){
        usleep(6000000);           //this should be 6 seconds
       // usleep(600000);

        Temp_ClientStillConnected = AllClients[In_ID_OfToHandleClient]->getIsClientStillConnected();



        if(Temp_ClientStillConnected){          //double checking, because status can be changed in 1 second
                                                //this solved the problem, that with heartbleed, the server terminated after a client disconnected because the server still tryed to send a Heartbleed


            MutexForOutputControll.lock();                   //this avoids a output mess at the terminal. i think i can delete that later, when i dont need the output of every heartbleed on the terminal
                AllClients[In_ID_OfToHandleClient]->askForHeartbleed(/*Heartbleed*/);
            MutexForOutputControll.unlock();
        }
    }
}



direction controller::convertStringToDirection(string In_Direction){//called from controller::interpretReceivedMessage()

    direction Temp = d_non;


    if      (In_Direction == "left"){
        Temp = d_left;
    }else if(In_Direction == "right"){
        Temp = d_right;
    }else if(In_Direction == "up"){
        Temp = d_up;
    }else if(In_Direction == "down"){
        Temp = d_down;
    }else if(In_Direction == "downleft" ){
        Temp =d_downleft;
    }else if(In_Direction == "downright"){
        Temp = d_downright;
    }else if(In_Direction == "upleft"){
        Temp = d_upleft;
    }else if(In_Direction == "upright"){
        Temp = d_upright;
    }else if(In_Direction == "upZ"){
        Temp = d_upZ;
    }else if(In_Direction == "downZ"){
        Temp = d_downZ;
    }else{
        exit(0);        //something went wrong
    }

    return Temp;

}



bool controller::areEqual(char In_Char1[], char In_Char2[]){       //copy pasted from client -> controller


    bool Output = true;


  //  int64_t SizeOfArray1 = sizeof(In_Char1);          //https://stackoverflow.com/questions/10735990/c-size-of-a-char-array-using-sizeof
    int64_t SizeOfArray1 = strlen(In_Char1);            // sizeof gave wrong number of characters
 //   int64_t SizeOfArray2 = sizeof(In_Char2);
    int64_t SizeOfArray2 = strlen(In_Char2);

    if(SizeOfArray1 != SizeOfArray2){
        Output = false;                 //the two arrays are not even from the same size;
    }else{


        for(int64_t I = 0; I < SizeOfArray1; I++){
            if(In_Char1[I] != In_Char2[I]){
                Output = false;                 //wenn mindestens ein buchstabe abweicht, dann sind die arrays nicht gleich und demnach wird false zurück gegeben
            }
        }
    }
    return Output;
}

int64_t controller::tryToLoginWith(string In_Password, string In_AccountName){ //called from receivedLoginRequest()

    int64_t Output = -1;
    if( AllUserAccounts.size() != 0 ){
        for( size_t I = 0; I < AllUserAccounts.size(); I++ ){
            if ( In_AccountName == AllUserAccounts[I].getUsername() ){
                if ( In_Password ==  AllUserAccounts[I].getPassword() ) {
                    Output = I;
                    cout << getTimestampInMiliseconds() << " requested login-name is already in the vector. place in vector: " << I << endl;
                }
            }
        }
    }
    cout << getTimestampInMiliseconds() << " end of controller::tryToLoginWith()" << endl;
    return Output;
}

void controller::confirmLoginProcess( int64_t In_IDofClient, int In_AccountID ){

    character* CharacterPointer = AllUserAccounts[In_AccountID].getPointerToCharacter();

    AllClients[In_IDofClient]->sendMessageToClient( "LoginSuccessfull;ClientID:*" + to_string(In_IDofClient) + "*" + CharacterPointer->getCharactername() + "*" + to_string( CharacterPointer->getEnergy() ) + "*ZRatio:" + to_string(Ratio_Z_RadiusToSightRadius) + "*WorldHeight:" + to_string(World.getWorldSize().Height) + "*WorldWidth:" + to_string(World.getWorldSize().Width) + + "*MaxLevelOverZero:" + to_string( World.getWorldSize().MaxLevelOverZero ) );
    //system("sleep 1");
    cout << getTimestampInMiliseconds() << " confirmLoginProcess(): login confirmed on client: " << to_string(In_IDofClient) << endl;

}


string controller::extract(PasswordOrAccountnameOrCharname In_PasswordOrAccountnameOrCharname, string In_PasswordAndUsernameAndCharname ){     //called from receivedCreateNewAccount()

        //this is also pasted to connectedClient::TryToLoginWith()
    string Output = "";
    int64_t temp_sizeOfString = In_PasswordAndUsernameAndCharname.size();
    char char_PasswordAndUsernameAndCharname[temp_sizeOfString];
    strcpy(char_PasswordAndUsernameAndCharname, In_PasswordAndUsernameAndCharname.c_str());
    bool UserNameComplete = false;
    bool PasswordComplete = false;
    char UserName[32];
    char Password[32];
    char Charname[32];
    bzero(UserName, 32);
    bzero(Password, 32);
    bzero(Charname, 32);
    int64_t SizeOfUserName = 0;
    int64_t J = 0;
    int64_t K = 0;
    for ( int64_t I = 0; I < temp_sizeOfString; I++ ){

        if( char_PasswordAndUsernameAndCharname[I] != '*' && UserNameComplete == false && PasswordComplete == false ){
            UserName[I] = char_PasswordAndUsernameAndCharname[I];
            SizeOfUserName++;
        }else if ( UserNameComplete == false && PasswordComplete == false ){
            UserNameComplete = true;
            I++;
        }
        if ( char_PasswordAndUsernameAndCharname[I] != '*'/*';'*/ && UserNameComplete == true && PasswordComplete == false ){

            Password[J] = char_PasswordAndUsernameAndCharname[I];
            J++;
        }
        if( UserNameComplete == true && char_PasswordAndUsernameAndCharname[I] == '*' ){
            PasswordComplete = true;
            I++;
        }

        if( char_PasswordAndUsernameAndCharname[I] != ';' && UserNameComplete == true && PasswordComplete == true ){
            Charname[K] = char_PasswordAndUsernameAndCharname[I];
            K++;
        }


    }

    string Str_Password = Password;
    string Str_UserName = UserName;
    string Str_Charname = Charname;

    if (In_PasswordOrAccountnameOrCharname == Enum_Password){
        Output = Str_Password;
    }
    if (In_PasswordOrAccountnameOrCharname == Enum_Accountname){
        Output = Str_UserName;
    }
    if (In_PasswordOrAccountnameOrCharname == Enum_Charname){
        Output = Str_Charname;
    }

    return Output;
}

bool controller::nameAlreadyInUse( string In_Username ){
    bool Output = false;
    for( size_t I = 0; I < AllUserAccounts.size() ; I++ ){
        if( AllUserAccounts[I].getUsername() == In_Username ){
            Output = true;
        }
    }
    return Output;
}

string controller::getTimestampInHoursAndMinutes(){


    //quick and dirty time-stamp: https://www.tutorials.de/threads/c-aktuelle-zeit-und-datum-in-datei-schreiben.279493/
    time_t Timestamp;
    tm *Now;
    Timestamp = time(0);
    Now = localtime(&Timestamp);
    string TimeString = "";
  //  string TimeString;
    int64_t Temp_Minutes = Now->tm_min;
    int64_t Temp_Hours = Now->tm_hour;
    if(Temp_Minutes > 9){
        TimeString = to_string(Temp_Hours) + ':' + to_string(Temp_Minutes);
    }else if(Temp_Minutes <= 9){                                    //this is needed, otherwise we would get "19:9" instead of "19:09"
        TimeString = to_string(Temp_Hours) + ':' + "0" + to_string(Temp_Minutes);
    }

    return TimeString;

}


void controller::receivedChatMessage( string In_ChatMessage, int64_t In_IDofClientSendingThisMessage ){
    int64_t Temp_AccountID = AllClients[In_IDofClientSendingThisMessage]->getAccountID();
    character* CharacterPointer = AllUserAccounts[Temp_AccountID].getPointerToCharacter();
    string Username = AllUserAccounts[Temp_AccountID].getUsername();
    string Charactername = CharacterPointer->getCharactername();

    cout << getTimestampInMiliseconds() << " a chat message arrived: "  << In_ChatMessage << endl;
    for(size_t I = 0; I < IdentifierForClients; I++){
        if( AllClients[I]->getIsClientStillConnected() ){
            string TimeStamp_str = "[" + getTimestampInHoursAndMinutes() + "]\t";
            string ToSendMessage = TimeStamp_str + Charactername  + "{" + Username + "}: " + In_ChatMessage;
            AllClients[I]->aNewChatMessageArrived( ToSendMessage );

        }
    }
}

void controller::receivedHeartbleedFromClient(int64_t In_ClientID){

    AllClients[In_ClientID]->ClientAnsweredHeartbleed();

    // on this place i need to send the client the message to turn the heart-symbol blue.
    AllClients[In_ClientID]->sendOrderToTurnHeartIntoBlue();
}


bool controller::receivedRequestToMove( string In_Direction, int64_t In_ClientID ){
    cout << getTimestampInMiliseconds() << " entering controller::receivedRequestToMove(): In_Direction: " << In_Direction << " ; " << "In_ClientID: " << In_ClientID << endl;

    bool Output = false;

    int64_t AccountID = AllClients[In_ClientID]->getAccountID();
    character* CharacterPointer = AllUserAccounts[AccountID].getPointerToCharacter();

    direction DirectionToMove = convertStringToDirection( In_Direction );

    bool CharacterIsAble = CharacterPointer->isCharacterAbleToMove();

    if( CharacterIsAble == true ){
        if( DirectionToMove == d_upZ ){
            if( CharacterPointer->getZeroGravity() == true ){       //jup, has zero gravity
                CharacterIsAble = true;
            }else{                                                  //na, has no zero gravity
                CharacterIsAble = false;
            }
        }
    }

    if( CharacterIsAble ){       // maybe the character is stunned

        position OldPositionOfCharacter = CharacterPointer->getPosition();
        position NewPositionOfCharacter = calculateNewPosition(OldPositionOfCharacter, DirectionToMove);

        //! alright... i changed the funtion calculateNewPosition() to not check anymore for other characters or any blocking cube on the new position.
        //! the return just calculates the new position no matter if the movement is possible. it only returns a -1;-1;-1 position if the new position would be not in the valid world.
        //! we just ask the World to add the character (which is a cube after all) to the new position. if that is possible, then this function will return
        //! 'true' and then we can ask the world to remove the character from the old position.
        //! if the to-add-function returned false, then there is obviously something blocking. so, we increase the z-dimension of the new position and try again.
        //! if that also fails, the step is not possible and we can leave this function

        if( NewPositionOfCharacter.PosY == -1 && NewPositionOfCharacter.PosX == -1 && NewPositionOfCharacter.PosZ == -1 ){

            //well.. calculateNewPosition() seemed to return an unvalid position to make clear, the the requestete position is not valid (out of world)
            return false;
        }

        //! at this point, the NewPosition is not already the new position of the character, but only the requested Position
        //! we need to ask the world on which position a step in the requested direction would end up.
        //! the world can return -1;-1;-1 for a not possible movement, because of a not possible movment (a wall or something)
        //! but in normal cases it will return a position which should be the smae as the current NewPositionOfCharacter in x and y dimension
        //! but can be a different one in z dimension

        NewPositionOfCharacter = World.getResultingPositionAfterTryingToMoveToNewPosition( NewPositionOfCharacter );

        if( NewPositionOfCharacter.PosY == -1 && NewPositionOfCharacter.PosX == -1 && NewPositionOfCharacter.PosZ == -1 ){
            //well.. getResultingPositionAfterTryingToMoveFromOldPositionToNewPosition() seemed to return an unvalid position to make clear, the requested movement can not be done because of a wall or something
            return false;
        }

        bool Succesfull = World.addCubeToCubestack( NewPositionOfCharacter, (cube*)CharacterPointer );

        /*
        if( Succesfull == false ){
            NewPositionOfCharacter.PosZ++;//trying the movement with a step up
            Succesfull = World.addCubeToCubestack( NewPositionOfCharacter, (cube*)CharacterPointer );
        }
        */

        if( Succesfull ){

            position VectorFromOldPositionToNewPosition = NewPositionOfCharacter - OldPositionOfCharacter;


            //do we need to extend the world?
            worldSize Temp_SizeOfWorld = World.getWorldSize();
            if( NewPositionOfCharacter.PosY >= (Temp_SizeOfWorld.Height - TRASHOLD_TO_EXTEND_WORLD_WHEN_CHARACTER_COMES_MIN_POSITIONS_TO_EDGE) ){
                extendTheWorld(d_down);
            }
            if( NewPositionOfCharacter.PosX >= (Temp_SizeOfWorld.Width - TRASHOLD_TO_EXTEND_WORLD_WHEN_CHARACTER_COMES_MIN_POSITIONS_TO_EDGE) ){
                extendTheWorld(d_right);
            }

            //cout << getTimestampInMiliseconds() << " starting to merge the AccountIDs which are effected by the movement" << endl;
            vector <int> AccountsWhoCanSeeTheOldPositionBeforeMovement = World.getIDsofAccountsWhoCanSeePosition(OldPositionOfCharacter);
            vector <int> AccountsWhoCanSeeTheNewPositionsAfterMovement = World.getIDsofAccountsWhoCanSeePosition(NewPositionOfCharacter);
            vector <int> MergedVector;
            MergedVector.reserve( AccountsWhoCanSeeTheOldPositionBeforeMovement.size() + AccountsWhoCanSeeTheNewPositionsAfterMovement.size() );
            MergedVector.insert( MergedVector.end(), AccountsWhoCanSeeTheOldPositionBeforeMovement.begin(), AccountsWhoCanSeeTheOldPositionBeforeMovement.end() );
            MergedVector.insert( MergedVector.end(), AccountsWhoCanSeeTheNewPositionsAfterMovement.begin(), AccountsWhoCanSeeTheNewPositionsAfterMovement.end() );
            sort( MergedVector.begin(), MergedVector.end() );
            MergedVector.erase( unique( MergedVector.begin(), MergedVector.end() ), MergedVector.end() );
            //cout << getTimestampInMiliseconds() << " merged" << endl;
            //!so now we should have a list of accountIDs which are effected by the change of position


            //!now we can remove the character from the old position
            World.removeCubeFromCubestack( OldPositionOfCharacter, (cube*)CharacterPointer );
            Output = true;

            CharacterPointer->setPosition( NewPositionOfCharacter );

            calculateDiffSightFieldAndSendToClientAndUpdatePosition( In_ClientID, VectorFromOldPositionToNewPosition, OldPositionOfCharacter );


            //!now its time to iterate over MergedVector and send all of them an update of the sightField
            position ZeroVector;
            ZeroVector.PosY = 0;
            ZeroVector.PosX = 0;
            ZeroVector.PosZ = 0;
            for(size_t I = 0; I < MergedVector.size(); I++){

                if( MergedVector[I] != AccountID ){   //without that, the to-handle-character would get two updates

                    int ClientIDofToUpdateUser = AllUserAccounts[ MergedVector[I] ].getIDofClientAttachedWithThisAccount(); //if client is not online any more, this function, will return '-1'
                    int AccountIDofToUpdateUser = MergedVector[I];
                    cout << "controller::receivedRequestToMove(): ClientIDofToUpdateUser = " << ClientIDofToUpdateUser << endl;
                    if( ClientIDofToUpdateUser != -1 && AllClients[ClientIDofToUpdateUser]->return_ClientIsConnected() == true ){      //! i guess actually one check should be enough

                        cout << getTimestampInMiliseconds() << " controller::receivedRequestToMove(): trying to calculateSightFieldAndSendToClientAndUpdatePosition() with ClientID: " << ClientIDofToUpdateUser << endl;

                        //!ok, the idea is to send a DiffSightField with only two cubes. the old position (from where the to-handle-character moved away) and the new position (where the character moved to)
                        vector< pair<position, cube*>> DiffSightField_ForOtherCharacter;

                        //!first we handle the old position:
                        pair<position, cube*> Temp_Pair_OldPosition;
                        Temp_Pair_OldPosition.first = OldPositionOfCharacter;
                        const vector<cube*>* Temp_CubeStack_OldPosition = World.getPointerToCubeStackOnPositionFromNormalWorld(OldPositionOfCharacter);
                        for( size_t CubeStackIndex = 0; CubeStackIndex < (*Temp_CubeStack_OldPosition).size(); CubeStackIndex++ ){
                            Temp_Pair_OldPosition.second = (*Temp_CubeStack_OldPosition)[CubeStackIndex];
                            DiffSightField_ForOtherCharacter.push_back(Temp_Pair_OldPosition);
                        }


                        //!then the new position:
                        //!so this is the time to check if the newPosition is actually visible for the other character
                        character* Pointer_ToUpdateCharacter = AllUserAccounts[AccountIDofToUpdateUser].getPointerToCharacter();
                        bool IsInSightRadius = Pointer_ToUpdateCharacter->isPositionInSightRadius( NewPositionOfCharacter );
                        if(IsInSightRadius){
                            pair<position, cube*> Temp_Pair_NewPosition;
                            Temp_Pair_NewPosition.first = NewPositionOfCharacter;
                            const vector<cube*>* Temp_CubeStack_NewPosition = World.getPointerToCubeStackOnPositionFromNormalWorld(NewPositionOfCharacter);
                            for( size_t CubeStackIndex = 0; CubeStackIndex < (*Temp_CubeStack_NewPosition).size(); CubeStackIndex++ ){
                                Temp_Pair_NewPosition.second = (*Temp_CubeStack_NewPosition)[CubeStackIndex];
                                DiffSightField_ForOtherCharacter.push_back(Temp_Pair_NewPosition);
                            }
                        }
                        string Temp_SerializedSightField = serializeSightField( ClientIDofToUpdateUser, AccountIDofToUpdateUser, &DiffSightField_ForOtherCharacter, false );
                        string ToSendMessage = "SightField------;" + Temp_SerializedSightField;
                        AllClients[ClientIDofToUpdateUser]->sendMessageToClient(ToSendMessage);

                    }else{
                        cout << getTimestampInMiliseconds() << " controller::receivedRequestToMove(): not entering calculateSightFieldAndSendToClientAndUpdatePosition() client " << ClientIDofToUpdateUser << " is offline!" << endl;
                    }
                }
            }


        }else{
            cout << getTimestampInMiliseconds() << " movement of User " << AccountID << " was not successfull. addCubeToCubestack returned false" << endl;

        }

    }else{
        cout << "character is not able to move." << endl;
    }
    cout << getTimestampInMiliseconds() << " leaving receivedRequestToMove()" << endl;
    return Output;
}

void controller::receivedCreateNewAccount( string In_PasswordAndAccountnameAndCharName, int64_t In_ClientID ){

    string Password = extract( Enum_Password, In_PasswordAndAccountnameAndCharName );
    string UserAccount = extract( Enum_Accountname, In_PasswordAndAccountnameAndCharName );
    string CharacterName = extract ( Enum_Charname, In_PasswordAndAccountnameAndCharName );

    if( nameAlreadyInUse( UserAccount ) == false ){


        AllUserAccounts.emplace_back( DEFAULT_SIGHT_RADIUS, AllUserAccounts.size() );
        int AccountID = AllUserAccounts.size()-1;
        AllUserAccounts[ AccountID ].setUserNameAndPasswordAndCharacterName( Password, UserAccount, CharacterName );

        character* CharacterPointer = AllUserAccounts[ AccountID ].getPointerToCharacter();


        position PositionToSetCharacter;
        PositionToSetCharacter = CharacterPointer->getPosition();
        while( World.isSomeCharacterSittingOnPosition(PositionToSetCharacter) ){
            PositionToSetCharacter.PosX++;
        }

        //!World.addCharacterToCubeStack(PositionToSetCharacter, AccountID);
        World.addCubeToCubestack( PositionToSetCharacter, (cube*)CharacterPointer );


        int64_t IndexOfThisNewAccountInVector = AllUserAccounts.size() - 1;
        cout << getTimestampInMiliseconds() << " new account has been created: " << In_PasswordAndAccountnameAndCharName << endl;
        //AllClients[In_ClientID]->confirmAccountCreationProcess();
        AllClients[In_ClientID]->sendMessageToClient("NuAccountCreated;ClientID:*" + to_string(In_ClientID) + "*" + CharacterPointer->getCharactername() + "*" + to_string( CharacterPointer->getEnergy() ) + "*ZRatio:" + to_string(Ratio_Z_RadiusToSightRadius) + "*WorldHeight:" + to_string(World.getWorldSize().Height) + "*WorldWidth:" + to_string(World.getWorldSize().Width) + "*MaxLevelOverZero:" + to_string( World.getWorldSize().MaxLevelOverZero )  );

        AllClients[In_ClientID]->setAccountID(IndexOfThisNewAccountInVector);
        AllUserAccounts[IndexOfThisNewAccountInVector].setIDofClientAttachedWithThisAccount(In_ClientID);

        //but after login, it would be nice to see something...
        cout << "but after login, it would be nice to see something... waiting for 1 second" << endl;
        system("sleep 1");

        buildSightFieldFromScratchAndUpdatePosition( In_ClientID );

    }else{
        //accountName already in use
        AllClients[In_ClientID]->tellClientUserAccountNameIsAlreadyInUse();
        cout << getTimestampInMiliseconds() << " controller::interpretReceivedmessage(): Client " << In_ClientID << " tryed to register an already existing user-name: " << UserAccount << endl;
    }
}


void controller::receivedLoginRequest( string In_PasswordAndUsername, int64_t In_ClientID ){
    bool TryAgain = true;
    cout << getTimestampInMiliseconds() << " controller::receivedLoginRequest(): received request to login from client " << In_ClientID << endl;
    string Password = extract( Enum_Password, In_PasswordAndUsername );
    string UserAccount = extract( Enum_Accountname, In_PasswordAndUsername );

    int64_t IndexOfAccountInVector = tryToLoginWith(Password, UserAccount);     //returnes ID of requested Account

    if( IndexOfAccountInVector == -1 ){
        cout << "controller::receivedLoginRequest(): requested Account doesn't exist" << endl;
    }

    while( TryAgain ){

        if( IndexOfAccountInVector != -1 ){
            cout << "controller::receivedLoginRequest(): enteryng Try-Again-loop. IndexOfAccountInVector: " << IndexOfAccountInVector << " ; AllUserAccounts[IndexOfAccountInVector].IsAlreadyAttachedToAClient(): " << AllUserAccounts[IndexOfAccountInVector].IsAlreadyAttachedToAClient() << endl;
        }
        if(  (IndexOfAccountInVector != -1) && ( !( AllUserAccounts[IndexOfAccountInVector].IsAlreadyAttachedToAClient() ) ) ) {
            //login was successfull

            //AllClients[In_ClientID].receivePointerToUserAccount( &(AllUserAccounts[IndexOfAccountInVector]), "controller::interpretReceivedMessage()" );
            AllClients[ In_ClientID ]->setAccountID( IndexOfAccountInVector );
            AllUserAccounts[ IndexOfAccountInVector ].setIDofClientAttachedWithThisAccount( In_ClientID );
            confirmLoginProcess( In_ClientID, IndexOfAccountInVector );


            //but after login, it would be nice to see something...
            buildSightFieldFromScratchAndUpdatePosition( In_ClientID );

            TryAgain = false;
        }else if(IndexOfAccountInVector == -1){        //if the login was not successfull
            //!when the requested account doesn't exist
            AllClients[In_ClientID]->sendInformationOfUnsuccessfullLogin( IndexOfAccountInVector );
            TryAgain = false;
        }else if( AllUserAccounts[IndexOfAccountInVector].IsAlreadyAttachedToAClient() ){
            //!when the requested account is still connected to a client
            int64_t IDofClientTheRequestedAccountIsAlreadyAttachedTo = AllUserAccounts[IndexOfAccountInVector].getIDofClientAttachedWithThisAccount();
            if( AllClients[IDofClientTheRequestedAccountIsAlreadyAttachedTo]->getIsClientStillConnected() ){
                AllClients[In_ClientID]->sendInformationOfUnsuccessfullLogin( IndexOfAccountInVector );       //the requested account is alrady in use by another client. ant that client ist sill online!!!
                TryAgain = false;
            }else{
                cout << "tryed to send information about failed login, but client seems to be offline! IndexOfAccountInVector: " << IndexOfAccountInVector << endl;
                TryAgain = false;
            }
        }else{
            cout << "it will try again" << endl;

        }
    }
}

void controller::receivedClientWantsToCloseConnection( int64_t In_ClientID ){
    cout << getTimestampInMiliseconds() << " controller::interpretReceivedMessage(): Client " << In_ClientID << " asked to close connection" << endl;
    AllClients[In_ClientID]->markClientAsDissconnected();
    AllUserAccounts[ AllClients[In_ClientID ]->getAccountID() ].unsetIDofClientAttacedWithThisAccount();
}

void controller::receivedPrimaryAttack( int In_IDofToAttackAccount, int In_ClientID ){

    cout << getTimestampInMiliseconds() << " entering controller::receivedPrimaryAttack" << endl;
    int IDofAttackingAccount = AllClients[In_ClientID]->getAccountID();
    character* CharacterPointerOfAttacker = AllUserAccounts[IDofAttackingAccount].getPointerToCharacter();

    character* CharacterPointerOfVictim = AllUserAccounts[ In_IDofToAttackAccount ].getPointerToCharacter();

    float PrimaryWeaponRange_Sqrt = -1;
    PrimaryWeaponRange_Sqrt = pow( CharacterPointerOfAttacker->getRangeOfPrimaryWeapon(), 2);


    position VectorBetweenAttackerAndVictim = CharacterPointerOfAttacker->getPosition() - CharacterPointerOfVictim->getPosition();
    if( PrimaryWeaponRange_Sqrt >= ( (pow(VectorBetweenAttackerAndVictim.PosY, 2)) + (pow(VectorBetweenAttackerAndVictim.PosX, 2)) + (pow(VectorBetweenAttackerAndVictim.PosZ, 2)) ) ){   //check if to-attack-character is in range of primary weapon
        cout << getTimestampInMiliseconds() << " controller::receivedPrimaryAttack(): Victim is in range" << endl;

        CharacterPointerOfVictim->receiveDamage( CharacterPointerOfAttacker->getHitpointsOfPrimaryWeapon() );

        if( AllUserAccounts[In_IDofToAttackAccount].getIDofClientAttachedWithThisAccount() != -1 ){  //check if the other account is online
            AllClients[ AllUserAccounts[In_IDofToAttackAccount].getIDofClientAttachedWithThisAccount() ]->sendMessageToClient( "UpdateEnergy----;" + to_string( CharacterPointerOfVictim->getEnergy() ) );
        }else{
            cout << getTimestampInMiliseconds() << " Victim is offline" << endl;
        }

        if( CharacterPointerOfVictim->getEnergy() < 0 ){
            //! missing: handle: Character is Dead.
        }
    }else{
        cout << getTimestampInMiliseconds() <<  " Character " << to_string( IDofAttackingAccount ) << "(" << CharacterPointerOfAttacker->getCharactername() << ") tryed to Attack, but Victim is out of Range " << endl;
    }
    cout << getTimestampInMiliseconds() << " leaving controller::receivedPrimaryAttack" << endl;

}



void controller::receivedSecondaryAttack( int In_IDofToAttackAccount, int In_ClientID){
    cout << getTimestampInMiliseconds() << " entering controller::receivedSecondaryAttack" << endl;
    int IDofAttackingAccount = AllClients[In_ClientID]->getAccountID();
    float SecondaryWeaponRange_Sqrt = -1;
    character* AttackingCharacterPointer = AllUserAccounts[ IDofAttackingAccount ].getPointerToCharacter();
    character* VictimCharacterPointer = AllUserAccounts[ In_IDofToAttackAccount ].getPointerToCharacter();

    SecondaryWeaponRange_Sqrt = pow( AttackingCharacterPointer->getRangeOfSecondaryWeapon(), 2);

    position VectorBetweenAttackerAndVictim = AttackingCharacterPointer->getPosition() - VictimCharacterPointer->getPosition();
    if( SecondaryWeaponRange_Sqrt >= ( (pow(VectorBetweenAttackerAndVictim.PosY, 2)) + (pow(VectorBetweenAttackerAndVictim.PosX, 2)) + (pow(VectorBetweenAttackerAndVictim.PosZ, 2)) ) ){
        cout << getTimestampInMiliseconds() << " controller::receivedSecondaryAttack(): Victim is in range" << endl;

        VictimCharacterPointer->receiveDamage( AttackingCharacterPointer->getHitpointsOfSecondaryWeapon() );

        if( AllUserAccounts[In_IDofToAttackAccount].getIDofClientAttachedWithThisAccount() != -1 ){     //check if the other account is online
            AllClients[ AllUserAccounts[In_IDofToAttackAccount].getIDofClientAttachedWithThisAccount() ]->sendMessageToClient( "UpdateEnergy----;" + to_string( VictimCharacterPointer->getEnergy() ) );
        }else{
            cout << getTimestampInMiliseconds() << " Victim is offline" << endl;
        }

        if( VictimCharacterPointer->getEnergy() < 0 ){
            //! missing: handle: Character is Dead.
        }

    }else{
        cout << getTimestampInMiliseconds() <<  " Character " << to_string( IDofAttackingAccount ) << "(" << AttackingCharacterPointer->getCharactername() << ") tryed to Attack, but Victim is out of Range " << endl;
    }
    cout << getTimestampInMiliseconds() << " leaving controller::receivedSecondaryAttack" << endl;

}

void controller::receivedAdminCommand( string In_PasswordAndCommand, int In_ClientID){

    cout << getTimestampInMiliseconds() << " entering controller::receivedAdminCommand()" << endl;

    //split In_Command:
    vector <string> Tokens = splitString( "*", In_PasswordAndCommand );

    string InputPassword;
    string InputCommand;




    if( Tokens.size() < 2 ){
        string ToSendMessage =  "AdmnCmmndMsngCmd;foo";                             //missing command
        AllClients[In_ClientID]->sendMessageToClient( ToSendMessage );
    }else if( Tokens.size() > 4 ){
        string ToSendMessage =  "AdmnCmmnd2MnyWrd;foo";                             //to many words
        AllClients[In_ClientID]->sendMessageToClient( ToSendMessage );


    }else{

        InputPassword = Tokens[0];
        InputCommand = Tokens[1];


        if ( InputPassword.size() > 16 ){
            string ToSendMessage =  "AdmnCmmndWrngPw-;" + InputCommand;             //wrong password
            AllClients[In_ClientID]->sendMessageToClient( ToSendMessage );

        }else{
            while( InputPassword.size() != 16 ){
                InputPassword += "-";
            }
            if ( InputPassword == ServerPassword ){                 //if correct password



                //!the importent part:
                if( InputCommand == "killServer" ){
                    cout << "\n" << getTimestampInMiliseconds() << " received command to kill server from client " << In_ClientID << ". good night!" << endl;
                    system("sleep 5");
                    exit(0);
                }else if( InputCommand == "broadcast" && Tokens.size() == 3 ){

                    string ToBroadcastMessage = Tokens[2];

                    broadcast(ToBroadcastMessage);

                }else if( InputCommand == "setSightRadius" && Tokens.size() == 4 ){

                    int NewSightRadius = stoi(Tokens[2]);
                    int ClientID = stoi(Tokens[3]);

                    AllUserAccounts[ AllClients[ClientID]->getAccountID() ].getPointerToCharacter()->setSightRadius(NewSightRadius);

                }else if( InputCommand == "getComplSightField" ){
                    cout << "received Admin-Command 'getComplSightField'" << endl;
                    buildSightFieldFromScratchAndUpdatePosition(In_ClientID);


                }else if( InputCommand == "setSolid" && Tokens.size() == 2 ){               //setting a 'solid'-cube on the position where the character stands right now. the character will be lifted up by on and a new sight-field will be calculcated
                    cout << "received Admin-Command 'setSolid'" << endl;
                    int AccountID = AllClients[In_ClientID]->getAccountID();
                    character* CharacterPointer = AllUserAccounts[AccountID].getPointerToCharacter();
                    position OldPositionOfCharacter = CharacterPointer->getPosition();


                    bool SuccesfullyMovedToTheRight = false;

                    SuccesfullyMovedToTheRight = receivedRequestToMove( "right", In_ClientID );

                    if( SuccesfullyMovedToTheRight ){
                        cube* Temp_SolidCube = new cube( 100, true, solid );
                        World.addCubeToCubestack( OldPositionOfCharacter, Temp_SolidCube );

                        buildSightFieldFromScratchAndUpdatePosition(In_ClientID);
                        //later, i should replace this funktion with a 'cheaper' version, as there is only one cube different now
                    }




                    //!if(SuccesfullyMovedToTheRight){

                    //!    receivedRequestToMove("right", In_ClientID);
                   //later, i should replace this funktion with a 'cheaper' version, as there is only one cube different now
                    //!}
                }else if( InputCommand == "upZ" && Tokens.size() == 2 ){
                    cout << "received Admin-Command 'up'" << endl;
                    int AccountID = AllClients[In_ClientID]->getAccountID();
                    character* CharacterPointer = AllUserAccounts[AccountID].getPointerToCharacter();
                    CharacterPointer->setZeroGravity( true );
                    receivedRequestToMove( "upZ", In_ClientID );
                    //buildSightFieldFromScratchAndUpdatePosition(In_ClientID);

                }else if( InputCommand == "downZ" && Tokens.size() == 2 ){
                    cout << "received Admin-Command 'up'" << endl;
                    int AccountID = AllClients[In_ClientID]->getAccountID();
                    character* CharacterPointer = AllUserAccounts[AccountID].getPointerToCharacter();
                    CharacterPointer->setZeroGravity( true );
                    receivedRequestToMove( "downZ", In_ClientID );
                    //buildSightFieldFromScratchAndUpdatePosition(In_ClientID);
                /*
                }else if( InputCommand == "clearIDCounter" && Tokens.size() == 2 ){
                    cout << "received Admin-Command 'clearIDCounter" << endl;
                    int AccountID = AllClients[In_ClientID]->getAccountID();
                    character* CharacterPointer = AllUserAccounts[AccountID].getPointerToCharacter();
                    int* PointerToCounter = CharacterPointer->getPointerTo_Debug_Counter_IDsAddedToCubes();
                    *PointerToCounter = 0;
                    cout << "*PointerToCounter: " << *PointerToCounter << endl;
                */
                }else{          //unknown command
                    string ToSendMessage = "unknownCommand--;" + InputCommand;
                    AllClients[In_ClientID]->sendMessageToClient( ToSendMessage );
                }

            }else{
                string ToSendMessage =  "AdmnCmmndWrngPw-;" + InputCommand;
                AllClients[In_ClientID]->sendMessageToClient( ToSendMessage );
            }
        }
    }

    cout << getTimestampInMiliseconds() << " leaving controller::receivedAdminCommand()" << endl;

}




struct_message controller::extractMessage(string In_OriginalMessage){
    cout << getTimestampInMiliseconds() << " entering controller:extractMessage()" << endl;

    vector<std::string> Fields;
    boost::split(Fields, In_OriginalMessage, boost::is_any_of(";"));


    struct_message Output;

    if( Fields.size() != 3 ){
        cout << getTimestampInMiliseconds() << " controller::extractMessage(): received strange message, not according to our protocoll: " << In_OriginalMessage << endl;
    }else{

        string Temp_MessageType = Fields[0];
        string Temp_MessageContent = Fields[1];
        string Temp_EndSymbol = Fields[2];


        Output.MessageType = Temp_MessageType;
        Output.MessageContent = Temp_MessageContent;
        Output.EndSymbol = Temp_EndSymbol;


    }

    cout << getTimestampInMiliseconds() << " leaving controller:extractMessage()" << endl;
    return Output;


}





vector <string> controller::splitString( string In_Seperator, string In_OriginalMessage){

    vector <string> Fields;
    boost::split(Fields, In_OriginalMessage, boost::is_any_of( In_Seperator ));

    return Fields;

}


void controller::calculateDiffSightFieldAndSendToClientAndUpdatePosition( int In_ClientID, position In_VectorFromOldPositionToNewPosition, position In_OldAbsolutePositionOfCharacter ){     //called from (for example controller::receivedRequestToMove() )

    //! Alright. new day, new function. the problem in the old one above is, that it took the most of the time to compare the
    //! old sightField ( before a move ) with the new one. but it is necessary to know which cubes can not be seen by the
    //! character any more. in the old version, i iterated through the old SightField and in every loop through the new one
    //! again, to detect if the cube from the old SightField is still in the New one. but actually that is not necessary
    //! because we actually know the movement of the character. and for a movement with length 1 (which are the most movements
    //! of course there are some with a bigger length, for example when falling of a steep hill but that movements can also
    //! be handled as a number of movements of length 1) we exactly know which cubes are new in the sightField and which can
    //! not be seen any more. in 2D there would be 8 possible ways to move, in 3D there are 26. so it is necessary to distinguish
    //! between those possibilities and then add or remove the information, that the cube can be seen.
    //! at the moment, the information that the character can see the cube is added to the cube in calculateSightForClient(). and
    //! and that is done to all the cube in SightField even if the information was already in the cube-stack. of course this is not
    //! very efficient. i guess the best thing is to remove the 'addIDofAAccountWhoCanSeeThisCube()' from calculateSightForClient()
    //! to this function where i will only change the cubes which are new to the sightField or not in sight any more.
    //! actually, i basically only need 6 possible directions, since i can build the other once out of them.

    //! at first: it is possible to get a movement-vector which has not at all the length of 1. so i need to divide that Vector
    //! into multiple basis-vectors and then do the same actions for all of this basis-vectors, which are one of the six directions
    //! i precalculated the sightLibrary::calculatePositionsToAddAfterMovement() and
    //! sightLibrary::calculatePositionsToRemoveAfterMovement()


    cout << getTimestampInMiliseconds() << " entering of controller::calculateSightFieldAndSendToClientAndUpdatePosition(): In_ClientID: " << In_ClientID << endl;

    int AccountID = AllClients[In_ClientID]->getAccountID();
    character* CharacterPointer = AllUserAccounts[AccountID].getPointerToCharacter();
    int SightRadius = CharacterPointer->getSightRadius();

    position WorkingCopyOfAbsolutePositionOfCharacter = In_OldAbsolutePositionOfCharacter;

    bool DoIt = false;

    //for movements which are bigger (or just "different") then the 6 predefined directions, we
    // split the movement up in a number of movements all consisting of the predefined directions ( "OneOfSixDirections")

    vector< pair<position, cube*>> DifferentialSightField;
    DifferentialSightField.reserve(512);

    int Limit = -1;             //Limit will contain the amount of base-vectors (in current dimension) which are
                                //needed to build the input vector. that sounds much more complicated than it is.
                                //vor example. we have a input-vector 0;1;-5 and we are are looking at the z-dimension
                                // -> that means Dimension_YXZ = 2 (because 0=Y, 1=X, 2=Z)
                                //.. so z-dimension it is.... if we strip down the z-dimension from the input-vector
                                // we get 0;0;-5
                                // that corresponds with a base-vector of 0;0;-1
                                // and the Limit will be '5' then.


    position BaseVector;        //the BaseVector will be used as a representation of one of the 6 standard directions

    //we are going though all dimensions here. first Y, then X, then Z
    for( size_t Dimension_YXZ = 0; Dimension_YXZ < 3; Dimension_YXZ++ ){        //3, because of the 3 dimensions

        Limit = -1;
        if( Dimension_YXZ == 0 ){                                              //means-> if the movement has a y-component
            if( In_VectorFromOldPositionToNewPosition.PosY > 0 ){       // in positive y-direction
                BaseVector.PosY = 1;
                BaseVector.PosX = 0;
                BaseVector.PosZ = 0;
                DoIt = true;
                Limit = In_VectorFromOldPositionToNewPosition.PosY;
            }else if( In_VectorFromOldPositionToNewPosition.PosY < 0 ){ // negative y-direction
                BaseVector.PosY = -1;
                BaseVector.PosX = 0;
                BaseVector.PosZ = 0;
                DoIt = true;
                Limit = abs(In_VectorFromOldPositionToNewPosition.PosY);
            }else{
                //debug("\tinput vector doesn't seem to have an y-dimension");
            }
        }else if( Dimension_YXZ == 1 ){
            if( In_VectorFromOldPositionToNewPosition.PosX > 0 ){       // positive x-direction
                BaseVector.PosY = 0;
                BaseVector.PosX = 1;
                BaseVector.PosZ = 0;
                DoIt = true;
                Limit = In_VectorFromOldPositionToNewPosition.PosX;
            }else if( In_VectorFromOldPositionToNewPosition.PosX < 0 ){ // negative x-direction
                BaseVector.PosY = 0;
                BaseVector.PosX = -1;
                BaseVector.PosZ = 0;
                DoIt = true;
                Limit = abs(In_VectorFromOldPositionToNewPosition.PosX);
            }else{
                //debug("\tinput vector doesn't seem to have an x-dimension");
            }
        }else if( Dimension_YXZ == 2 ){
            if( In_VectorFromOldPositionToNewPosition.PosZ > 0 ){       // positive z-direction
                BaseVector.PosY = 0;
                BaseVector.PosX = 0;
                BaseVector.PosZ = 1;
                DoIt = true;
                Limit = In_VectorFromOldPositionToNewPosition.PosZ;
            }else if( In_VectorFromOldPositionToNewPosition.PosZ < 0 ){ // negative z-direction
                BaseVector.PosY = 0;
                BaseVector.PosX = 0;
                BaseVector.PosZ = -1;
                DoIt = true;
                Limit = abs(In_VectorFromOldPositionToNewPosition.PosZ);
            }else{
                //debug("\tinput vector doesn't seem to have an z-dimension");
            }
        }

        if( DoIt ){



            vector < position >* PositionsToBeAdded;
            vector < position >* PositionsToRemove;

            for( int DeltaCounter = 0; DeltaCounter < Limit; DeltaCounter++ ){  //make that for each length of 1 in the current dimension (y,x,z) one time:

                debug( "\tentering for-loop with DeltaCounter: " + to_string(DeltaCounter) + " ; Limit: " + to_string(Limit) );

                //! because i have some positions in "PositionsToBeAdded" aswell as in "PositionsToRemove" i guess its best to first remove and then add again.

                //going through all cubes, which can not be seen by the character any more and remove that infomation from the cube..
                //!PositionsToRemove = SightLibrary.getPointerToPositionsToBeRemovedFromSightField( SightRadius, BaseVector );
                PositionsToRemove = SightLibrary.getPointerToPositionsToBeAddedToSightField( SightRadius, BaseVector*(-1) );       //actually, we don't need the hole stuff with the "to-Remove-Position" because we can just use the to-add directions in reverse directions
                for( size_t PositionsIndex = 0; PositionsIndex < (*PositionsToRemove).size(); PositionsIndex++ ){
                    //! the next two lines can be wrapped together later.
                    position Temp_CurrentRelativePosition = (*PositionsToRemove)[PositionsIndex];
                    position Temp_CurrentAbsolutePosition = Temp_CurrentRelativePosition + WorkingCopyOfAbsolutePositionOfCharacter;

                    if( World.positionIsValid( Temp_CurrentAbsolutePosition ) ){
                        World.removeIDofAAccountWhoCanSeePosition( AccountID, Temp_CurrentAbsolutePosition );
                    }
                }


                WorkingCopyOfAbsolutePositionOfCharacter = WorkingCopyOfAbsolutePositionOfCharacter + BaseVector;       //so.. after partial movement
                //! it turned out, that this is the right place to do that. in the past, i did it even before the to-remove-position-handling (right just before the for-loop above)
                //! that caused the removeIDofAAccountWhoCanSeePosition()-function to add positions, which are actually ahead of us... anyway. this seems to be the right place

                PositionsToBeAdded = SightLibrary.getPointerToPositionsToBeAddedToSightField( SightRadius, BaseVector );
                for( size_t PositionsIndex = 0; PositionsIndex < (*PositionsToBeAdded).size(); PositionsIndex++ ){
                    position Temp_CurrentRelativePosition = (*PositionsToBeAdded)[PositionsIndex];
                    position Temp_CurrentAbsolutePosition = Temp_CurrentRelativePosition + WorkingCopyOfAbsolutePositionOfCharacter;
                    if( World.positionIsValid(Temp_CurrentAbsolutePosition) ){

                        const vector < cube* >* Temp_Cubestack = World.getPointerToCubeStackOnPositionFromNormalWorld(Temp_CurrentAbsolutePosition);
                        if( Temp_Cubestack != NULL ){ //! -> yep, it is possible to get an empty cube-stack
                            for( size_t CubeStackIndex = 0; CubeStackIndex < (*Temp_Cubestack).size(); CubeStackIndex++ ){
                                pair<position, cube*> Temp_Pair;
                                Temp_Pair.first = Temp_CurrentAbsolutePosition;
                                Temp_Pair.second = (*Temp_Cubestack)[CubeStackIndex];
                                DifferentialSightField.push_back(Temp_Pair);
                            }
                        }else{
                            //cout << "yep, it is possible. exit now." << endl;
                            //exit(0);
                        }
                        World.addIDofAAccountWhoCanSeePosition( AccountID, Temp_CurrentAbsolutePosition );
                        //!#ifdef USE_COMPLICATED_VOODOO
                        if( Temp_CurrentAbsolutePosition.PosY >= 0 && Temp_CurrentAbsolutePosition.PosX >= 0 && Temp_CurrentAbsolutePosition.PosZ >= 0 && Temp_CurrentAbsolutePosition.PosZ < MAX_LEVEL_OVER_ZERO ){
                            World.addIDto_ClientsSawThisPositionInThisSession( In_ClientID, Temp_CurrentAbsolutePosition );
                            AllClients[In_ClientID]->addIDto_ListOfPositionsThisAccountSawInThisSession( World.getHeadPointerFromPosition(Temp_CurrentAbsolutePosition) );
                        }
                        //!#endif // USE_COMPLICATED_VOODOO
                    }
                }
                //cout << "done with PositionsToBeAdded" << endl;
            }
            //cout << "done for all in 'Limit'" << endl;
        }
    }


    // so by now, the SightField should be up to date an we can serialize it now. and send it to the client
    string SerializedSightField = serializeSightField(
                                                        In_ClientID,
                                                        AccountID,
                                                        &DifferentialSightField,
                                                        //!#ifndef USE_COMPLICATED_VOODOO
                                                        //!false   //do not filter cubes -> none-cubes remain if not using the voodoo-methode
                                                        //!#endif // USE_COMPLICATED_VOODOO
                                                        //!#ifdef USE_COMPLICATED_VOODOO
                                                        true    // if using the voodoo-methode, please do filter
                                                        //!#endif // USE_COMPLICATED_VOODOO
                                                      );

    //! i used to set the third parameter here to 'true' to avoid sending tonnes of 'none'-cubes.
    //! but that caused this problem: after a character saw a cube and moved on, but not to far
    //! ( the curses-pad (client-side) hasn't been cleared ) but the cube got out of active sightField
    //! then the cube changed or vanished (after a movement of an other character, very likely) and then
    //! the character moved back, he still saw this cube. because the cube turned into 'none' and then
    //! got filtered by this function. a quick work around is to set this boolean to 'false', but that
    //! could be cause long calculating-times on both sides. i need to check that out...
    //! well, it seems to be reasonably fast on a raspberry pi ( i made a quick-and-dirty-test, the difference
    //! would be about 6,7 ms instead of 9,5 ms. hm... ), as long the ping is low enough. that seems to be a
    //! smaller bottle-neck.
    //! but i need to keep that in mind. i don't know any other elegant solution for that problem yet.
    //! one idea is: i need a map of pairs of int and bool for each position in the world. in this int, i save
    //! the Account-ID of each Account who saw the position since his last login. the bool will be 'false' by default
    //! and only set 'true' if a cube on that position changes. then, i need to check for each 'none' cube
    //! while serializing whether that position has changed since the account saw the cube.
    //! if so, the 'none' cube should be serialized anyway (even, if filtering is off. maybe filtering is not necessary then anymore at all)
    //! and the bool set to 'false' again.
    //! when account is logging off. all this IDs in the maps should be deleted, to avoid a slowing down the server after a while
    //! for that, every account needs a vector with pointers to the positions, which the character saw in this session.
    //! so i can just iterate over this vector and delete the pair from the map again.
    //! but that is complicated and not very elegant.
    //! all of that is called Complicated Voodoo
    //!

    string ToSendMessage = "SightField------;" + SerializedSightField;

    AllClients[In_ClientID]->sendMessageToClient(ToSendMessage);



    //debug( getTimestampInMiliseconds() + " end of controller::calculateSightFieldAndSendToClientAndUpdatePosition()" );
    cout << getTimestampInMiliseconds() << " end of controller::calculateSightFieldAndSendToClientAndUpdatePosition()" << endl;
}


void controller::buildSightFieldFromScratchAndUpdatePosition( int In_ClientID ){

    cout << getTimestampInMiliseconds() << " entering controller::buildSightFieldFromScratch()" << endl;

    int AccountID = AllClients[In_ClientID]->getAccountID();
    character* CharacterPointer = AllUserAccounts[AccountID].getPointerToCharacter();

    //! it might be possible that this function gets called in the middle of the game
    //! for example after a port to a completly different position. in that case the
    //! old sightField hast still cubes and those cubes still got the information,
    //! that they can be seen by the character. so this is the right place to delete
    //! this information. in most cases (right after logging in or crateing a new account)
    //! the sightField is empty and will not be executed any way



    //!i guess, what is missing here, is that i take the Sightfield as it was until here and just completely clear it... and in the next block
    //! at the right time, i add the IDs back to the positions in currentWorld






    //!calculateSightForClient(In_ClientID, true); i move that from the function directly in here... because otherwise, this function isn't doing much but producing overhead
    position PositionOfCharacter = CharacterPointer->getPosition();
    int SightRadius = CharacterPointer->getSightRadius();
    vector< pair< position, cube*> >* SightFieldPointer = CharacterPointer->getSightFieldPointer();
    SightFieldPointer->clear();
    //worldSize WorldSize = World.getWorldSize();
    vector < vector < position > >* PointerToSightFieldBluePrint = SightLibrary.returnPointerToSightFieldBlueprint( SightRadius );
    position Temp_RelativePosition;
    position Temp_AbsolutePosition;
    const vector < cube* >* Temp_CubeStack;
    for( size_t ShellNumber = 0; ShellNumber < PointerToSightFieldBluePrint->size(); ShellNumber++ ){
        for( size_t Index = 0; Index < (*PointerToSightFieldBluePrint)[ShellNumber].size(); Index++ ){
            Temp_RelativePosition =  (*PointerToSightFieldBluePrint)[ShellNumber][Index];
            Temp_AbsolutePosition = Temp_RelativePosition + PositionOfCharacter;
            if( World.positionIsValid( Temp_AbsolutePosition )
                                                                            ){
                    Temp_CubeStack = World.getPointerToCubeStackOnPositionFromNormalWorld( Temp_AbsolutePosition );

                    for( size_t CubeStackIndex = 0; CubeStackIndex < Temp_CubeStack->size(); CubeStackIndex++ ){
                        pair<position, cube*> Temp_Pair;
                        Temp_Pair.first = Temp_AbsolutePosition;
                        Temp_Pair.second = (*Temp_CubeStack)[CubeStackIndex];
                        (*SightFieldPointer).push_back( Temp_Pair );

                        World.addIDofAAccountWhoCanSeePosition( AccountID, Temp_AbsolutePosition/*, CharacterPointer->getPointerTo_Debug_Counter_IDsAddedToCubes()*/ );

                        //!#ifdef USE_COMPLICATED_VOODOO
                        if( Temp_AbsolutePosition.PosY >= 0 && Temp_AbsolutePosition.PosX >= 0 && Temp_AbsolutePosition.PosZ >= 0 && Temp_AbsolutePosition.PosZ < MAX_LEVEL_OVER_ZERO ){
                            World.addIDto_ClientsSawThisPositionInThisSession(In_ClientID, Temp_AbsolutePosition);
                            AllClients[In_ClientID]->addIDto_ListOfPositionsThisAccountSawInThisSession( World.getHeadPointerFromPosition(Temp_AbsolutePosition) );
                        }
                        //!#endif // USE_COMPLICATED_VOODOO
                    }
            }else{
              //  cout << "Position out of Range: " << Temp_AbsolutePosition << endl;
            }

        }
    }
















    string Temp_SerializedSightField = serializeSightField(
                                                            In_ClientID,
                                                            AccountID,
                                                            CharacterPointer->getSightFieldPointer(),
                                                            //true
                                                            //!#ifndef USE_COMPLICATED_VOODOO
                                                            //!false
                                                            //!#endif
                                                            //!#ifdef USE_COMPLICATED_VOODOO
                                                            true
                                                            //!#endif // USE_COMPLICATED_VOODOO

                                                           );
    //string ToSendMessage = "SightFieldCompl-;" + Temp_SerializedSightField;
    // actually, there is no difference between a sightField from scratch at client sight
    string ToSendMessage = "SightField------;" + Temp_SerializedSightField;

    AllClients[In_ClientID]->sendMessageToClient(ToSendMessage );


    cout << getTimestampInMiliseconds() << " leaving controller::buildSightFieldFromScratch()" << endl;


}


//this function gets called in threads...
void controller::serializeSightField_subFunction(
                                                    //!#ifdef USE_COMPLICATED_VOODOO
                                                    int In_ClientID,
                                                    //!#endif // USE_COMPLICATED_VOODOO
                                                    character* In_CharacterPointer, string* In_Output, vector< pair< position, cube*>>* In_SightField, size_t In_DoItFrom, size_t In_Until,  bool In_Filter_NoneCubes){  //gets called in multiple threads from serializeSightField()



    string Temp_Output = "";
    cube* Temp_Cube;
    string Head = "";
    bool PlacedSomethingFromCubestackInOutput = false;
    for( size_t Index = In_DoItFrom; Index < In_Until; Index++ ){
        PlacedSomethingFromCubestackInOutput = false;
        Temp_Cube = (*In_SightField)[Index].second;
        standardCube Temp_CubeType = Temp_Cube->getTypeOfCube();


        //!#ifdef USE_COMPLICATED_VOODOO
        position Position = (*In_SightField)[Index].first;
        bool CubeStackInThatPlaceHasChanged = false;


        if( Position.PosY >= 0 && Position.PosX >= 0 && Position.PosZ >= 0 && Position.PosZ < MAX_LEVEL_OVER_ZERO  ){
            map< int, bool >* Map_ClientsSawThisPositionInThisSession = World.getPointerTo_ClientsSawThisPositionInThisSession(Position);
            //checking if the account id is already in the vector. that would mean, the account saw this position in this session
            if( Map_ClientsSawThisPositionInThisSession->count(In_ClientID) ){
                CubeStackInThatPlaceHasChanged = (*Map_ClientsSawThisPositionInThisSession)[In_ClientID];
                (*Map_ClientsSawThisPositionInThisSession)[In_ClientID] = false;  //it will be definitly up to date after this
            }
        }
        //!#endif // USE_COMPLICATED_VOODOO



        if(
            Temp_CubeType != none
            || In_Filter_NoneCubes == false
            //!#ifdef USE_COMPLICATED_VOODOO
            || CubeStackInThatPlaceHasChanged
            //!#endif // USE_COMPLICATED_VOODOO
            ){           //normally we don't want to submit "none"-cubes. but in case some cases it is needed. for example if there is a cube in sightField which changed type (that is usually the case when a character moves)
            position Position = (*In_SightField)[Index].first;
            PlacedSomethingFromCubestackInOutput = true;
            string Temp_TypeOfCube = Temp_Cube->getStringOfCubeType();
            Head = "%Y:" + to_string(Position.PosY) + "*X:" + to_string(Position.PosX) + "*Z:" + to_string(Position.PosZ);
            Temp_Output = Temp_Output + Head + "!" + Temp_TypeOfCube;
            if( Temp_TypeOfCube == "characterCube" ){
                //!Temp_Output += "*" + to_string( Temp_Cube->getIDofAccountSittingAtThisCube() );
                Temp_Output += "*" + to_string( ((character*)Temp_Cube)->getIDofAccountThisCharacterBelongsTo() );
                //!int Temp_IDofAccountOfCharacterCube = Temp_Cube->getIDofAccountSittingAtThisCube();
                int Temp_IDofAccountOfCharacterCube = ((character*)Temp_Cube)->getIDofAccountThisCharacterBelongsTo();
                Temp_Output += "*" + In_CharacterPointer->getCharactername() + "*" + AllUserAccounts[ Temp_IDofAccountOfCharacterCube ].getUsername();
            }
            Temp_Output+= "!" + to_string( Temp_Cube->getEnergy() ) + "!";
        }



        if(PlacedSomethingFromCubestackInOutput){
            Temp_Output += "%";
        }
    }

    Serialize_Mutex.lock();
    (*In_Output) += Temp_Output;
    Serialize_Mutex.unlock();

}


string controller::serializeSightField( int In_ClientID, int In_AccoutID, vector< pair< position, cube*>>* In_SightField, bool In_Filter_NoneCubes ){

    cout << getTimestampInMiliseconds() << " entering controller::serializeDifferentialSightField()" << endl;
    string Output = "";
    character* CharacterPointer = AllUserAccounts[In_AccoutID].getPointerToCharacter();
    int SightRadius = CharacterPointer->getSightRadius();

    Output = "R:" + to_string(SightRadius);
    Output += "*" + CharacterPointer->getStringOfPosition();


    //!serializing in 2 Threads
    #if SPLIT_SERIALIZING_INTO_THREADS == 1
    serializeSightField_subFunction( In_ClientID, CharacterPointer, &Output, In_SightField, 0, size_t( (*In_SightField).size() ), In_Filter_NoneCubes );
    #endif // SPLIT_SERIALIZING_INTO_THREADS

    #if SPLIT_SERIALIZING_INTO_THREADS == 2    //2 threads
    size_t Stop_1 = size_t( (*In_SightField).size() / 2  );
    size_t Stop_2 = size_t( (*In_SightField).size() );

    thread Thread_1( &controller::serializeSightField_subFunction, this,
                                                                            //!#ifdef USE_COMPLICATED_VOODOO
                                                                            In_ClientID,
                                                                            //!#endif // USE_COMPLICATED_VOODOO
                                                                            CharacterPointer, &Output, In_SightField, 0, Stop_1, In_Filter_NoneCubes );
    thread Thread_2( &controller::serializeSightField_subFunction, this,
                                                                            //!#ifdef USE_COMPLICATED_VOODOO
                                                                            In_ClientID,
                                                                            //!#endif // USE_COMPLICATED_VOODOO
                                                                            CharacterPointer, &Output, In_SightField, Stop_1, Stop_2, In_Filter_NoneCubes );
    Thread_1.join();
    Thread_2.join();
    #endif // SPLIT_SERIALIZING_INTO_2_THREADS

    #if SPLIT_SERIALIZING_INTO_THREADS == 4   //4 threads
    size_t Stop_1 = size_t( (*In_SightField).size() * 0.25  );
    size_t Stop_2 = size_t( (*In_SightField).size() * 0.5 );
    size_t Stop_3 = size_t( (*In_SightField).size() * 0.75 );
    size_t Stop_4 = size_t( (*In_SightField).size() * 1 );

    thread Thread_1( &controller::serializeSightField_subFunction, this,
                                                                            //!#ifdef USE_COMPLICATED_VOODOO
                                                                            In_ClientID,
                                                                            //!#endif // USE_COMPLICATED_VOODOO
                                                                            CharacterPointer, &Output, In_SightField, 0, Stop_1, In_Filter_NoneCubes );
    thread Thread_2( &controller::serializeSightField_subFunction, this,
                                                                            //!#ifdef USE_COMPLICATED_VOODOO
                                                                            In_ClientID,
                                                                            //!#endif // USE_COMPLICATED_VOODOO
                                                                            CharacterPointer, &Output, In_SightField, Stop_1, Stop_2, In_Filter_NoneCubes );
    thread Thread_3( &controller::serializeSightField_subFunction, this,
                                                                            //!#ifdef USE_COMPLICATED_VOODOO
                                                                            In_ClientID,
                                                                            //!#endif // USE_COMPLICATED_VOODOO
                                                                            CharacterPointer, &Output, In_SightField, Stop_2, Stop_3, In_Filter_NoneCubes );
    thread Thread_4( &controller::serializeSightField_subFunction, this,
                                                                            //!#ifdef USE_COMPLICATED_VOODOO
                                                                            In_ClientID,
                                                                            //!#endif // USE_COMPLICATED_VOODOO
                                                                            CharacterPointer, &Output, In_SightField, Stop_3, Stop_4, In_Filter_NoneCubes );
    Thread_1.join();
    Thread_2.join();
    Thread_3.join();
    Thread_4.join();
    #endif // SPLIT_SERIALIZING_INTO_2_THREADS






    cout << getTimestampInMiliseconds() << " leaving controller::serializeDifferentialSightField()" << endl;
    return Output;
}



position controller::calculateNewPosition(position In_OldPosition, direction In_Direction){        //gets called from receivedRequestToMove()
//if the requested Movement is allowed, this funktion returns the new position (can also be a Position in an other z-level);

    cout << getTimestampInMiliseconds() << " entering controller::calculateNewPosition() with In_OldPosition: " << In_OldPosition << endl;

    position Output;

    position ToCheckPositionInWorld = In_OldPosition;

    if (In_Direction == d_left){
        ToCheckPositionInWorld.PosX--;
        cout << "In_Direction == d_left" << endl;
    }else if(In_Direction == d_right){
        ToCheckPositionInWorld.PosX++;
        cout << "In_Direction == d_right" << endl;
    }else if(In_Direction == d_up){
        ToCheckPositionInWorld.PosY--;
        cout << "In_Direction == d_up" << endl;
    }else if(In_Direction == d_down){
        ToCheckPositionInWorld.PosY++;
        cout << "In_Direction == d_down" << endl;
    }else if(In_Direction == d_downleft){
        ToCheckPositionInWorld.PosX--;
        ToCheckPositionInWorld.PosY++;
        cout << "In_Direction == d_downleft" << endl;
    }else if(In_Direction == d_downright){
        ToCheckPositionInWorld.PosX++;
        ToCheckPositionInWorld.PosY++;
        cout << "In_Direction == d_downright" << endl;
    }else if(In_Direction == d_upleft){
        ToCheckPositionInWorld.PosX--;
        ToCheckPositionInWorld.PosY--;
        cout << "In_Direction == d_upleft" << endl;
    }else if(In_Direction == d_upright){
        ToCheckPositionInWorld.PosX++;
        ToCheckPositionInWorld.PosY--;
        cout << "In_Direction == d_upright" << endl;
    }else if(In_Direction == d_upZ){
        ToCheckPositionInWorld.PosZ++;
        cout << "In_Direction == d_upZ" << endl;
        return ToCheckPositionInWorld;
    }else if(In_Direction == d_downZ){
        ToCheckPositionInWorld.PosZ--;
        cout << "In_Direction == d_downZ" << endl;
    }



    if( ToCheckPositionInWorld.PosY >= 0 && ToCheckPositionInWorld.PosX >= 0 && ToCheckPositionInWorld.PosZ >= 0 ){

        //!debug
        cout << getTimestampInMiliseconds() << " ToCheckPositionInWorld: " << ToCheckPositionInWorld << endl;
        //!

        Output = ToCheckPositionInWorld;

        //!i remove that here, because i change the design. from now on, i just calculate the possible position, no matter if there is something blocking
        //! what i try to write the character into the world, which is not possible if there is another blocking cube on the cubestack, then i
        //! add +1 to the z-dimension and try again. if that also fails, the movement doesn't seem to be possilbe.
        //! oh, and by the way: walking upon another character will be possible, since a caracter is a cube aswell..
        /*!
        vector < cube* > Temp_CubesOnThisPosition = World.getCubesOnPositionFromNormalWorld(ToCheckPositionInWorld);
        bool ThereIsSomethingBlocking = false;
        for( size_t I = 0; I < Temp_CubesOnThisPosition.size(); I++ ){

            //if there is a other character right in front of the to-handle character. it is pretty clear, that the character can not pass
            //! ... well i actually remove that, it should be possible to jump upon another character
            //!if( Temp_CubesOnThisPosition[I]->getTypeOfCube() == characterCube ){
            //!    return In_OldPosition;
            //!}
            //!

            if( Temp_CubesOnThisPosition[I]->isFlat() == true ){
                Output = ToCheckPositionInWorld;
            }else{                                  //it seems, there is a wall right in front the character
                ThereIsSomethingBlocking = true;
                Output = In_OldPosition;

            }
        }

        if( ThereIsSomethingBlocking ){
            position PositionAbove = ToCheckPositionInWorld;
            PositionAbove.PosZ++;
            vector < cube* > Temp_CubesAbove = World.getCubesOnPositionFromNormalWorld( PositionAbove );
            bool PositionAboveIsOnlyFlat = true;
            for( size_t I = 0; I < Temp_CubesAbove.size() && PositionAboveIsOnlyFlat == true; I++ ){
                if( Temp_CubesAbove[I]->isFlat() == false ){                               //but if the cube right obove the aimed cube isFlat(), than it is possible by increasing the z-position. ( compare with a step )
                    PositionAboveIsOnlyFlat = false;
                }
            }
            if( PositionAboveIsOnlyFlat == true ){
                Output = PositionAbove;
            }
        }
        */



        //!checking for stuff right below the character:
        /*! i try to move that also in the world-class
        bool FoundSolidGround = false;
        position Temp_PositionBelowCharacter = ToCheckPositionInWorld;
        while(FoundSolidGround == false){
            Temp_PositionBelowCharacter.PosZ--;
            vector <cube*> StuffBelowCharacter = World.getCubesOnPositionFromNormalWorld(Temp_PositionBelowCharacter);
            for( size_t I = 0; I < StuffBelowCharacter.size() && FoundSolidGround == false; I++ ){

                if( StuffBelowCharacter[I]->isFlat() == false ){
                    FoundSolidGround = true;
                }
            }
            Output.PosZ--;
        }
        Output.PosZ++;
    */
    }else{
        //cout << getTimestampInMiliseconds() << " requested Position is not valid!: ToCheckPositionInWorld: " << ToCheckPositionInWorld.PosY << ";" << ToCheckPositionInWorld.PosX << ";" << ToCheckPositionInWorld.PosZ << endl;
        position NonValidPosition;
        Output = NonValidPosition;
    }


    cout << getTimestampInMiliseconds() << " leaving controller::calculateNewPosition(): Output: " << Output << endl;


    return Output;

}

string controller::getTimestampInMiliseconds(){

    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    string Output = to_string(ms);
    Output.erase( 0, 4 );

    return Output;
}



void controller::extendTheWorld( direction In_ExtendInDirection ){     //direction must be 'd_down' or 'd_right'
    cout << getTimestampInMiliseconds() << " entering controller::extendTheWorld()" << endl;
    World.extendTheWorldWithFlatLand( HOW_MUCH_TO_EXTEND_THE_WORLD_WHEN_CHARACTER_COMES_TO_THE_EDGE, In_ExtendInDirection);
    cout << getTimestampInMiliseconds() << " leaving controller::extendTheWorld()" << endl;
}


void controller::broadcast( string In_ToBroadcastMessage ){

    cout << getTimestampInMiliseconds() << " entering controller::broadcast. In_ToBroadcastMessage: " << In_ToBroadcastMessage << endl;

    string ToSendMessage =  "broadcast-------;" + In_ToBroadcastMessage;
    for( int Index = 0; Index < NUMBER_OF_CLIENTS; Index++ ){

        //if( AllClients[Index]->getIsClientStillConnected() == true ){    //i already check that in client::sendMessageToClient()

        AllClients[Index]->sendMessageToClient( ToSendMessage );

        //}

    }

    cout << getTimestampInMiliseconds() << " leaving controller::broadcast" << endl;

}


void controller::debug(string In_Put){

    //!Debug
    timeb Tb;
    ftime (&Tb);
    int64_t TimeStamp = Tb.millitm + (Tb.time & 0xfffff) * 1000;
    fstream Fstream;                          //this writes the debug.log
    Fstream.open("debug.log", ios::out|ios::app );
    Fstream << "[" <<to_string(TimeStamp) << "]: " << In_Put << endl;
    Fstream.close();
}


