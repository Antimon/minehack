#include "crafting.h"

#include <string>

using namespace std;

crafting::crafting( string In_NameOfCraftingAction ){
    Name = In_NameOfCraftingAction;
}

crafting::~crafting(){
    //dtor
}

/*
crafting::crafting(const crafting& other){
    //copy ctor
}
*/

crafting& crafting::operator=(const crafting& rhs){
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}


void crafting::setCraftingTool( craftingTool* In_ToUseTool ){
    UsedTool = In_ToUseTool;

}
