#ifndef CUBE_H_INCLUDED
#define CUBE_H_INCLUDED

#include <vector>
#include <map>


//!will be needed later for saving stuff
//!#include <../include/cereal/archives/json.hpp>
//!#include <../include/cereal/types/vector.hpp>      //stuff for json. we might not need all of them.


//#include "position.h"

using namespace std;

typedef enum {
                none = 0,           //isFlat = true
                stone = 1,          //isFlat = ? ( not defined yet. do in: cube::setTypeOfCube() )
                trunk = 2,          //isFlat = false
                water = 3,          //isFlat = false
                grass = 4,          //isFlat = true
               // blank = 5,
                solid = 6,          //isFlat = false
                branch = 7,         //isFlat = true
                leafsAndTwigs = 8,  //isFlat = true
                sand = 9,           //isFlat = true

                characterCube = 10,       //for example a character. or maybe an animal???
                                    //isFlat = false

                gravel = 11         // isFlat = true


              /*  template <class Archive>
                    void serialize(Archive & archive){
                    archive( CEREAL_NVP(nane), CEREAL_NVP(stone), CEREAL_NVP(trunk), CEREAL_NVP(water), CEREAL_NVP(water), CEREAL_NVP(grass), CEREAL_NVP(solid)  ); // serialize things by passing them to the archive
                }*/

                            }standardCube;



/*
typedef struct {
    int PosY = -1;
    int PosX = -1;
    int PosZ = -1;
    template <class Archive>
        void serialize(Archive & archive){
        archive( CEREAL_NVP(PosY), CEREAL_NVP(PosX), CEREAL_NVP(PosZ) ); // serialize things by passing them to the archive
    }
}position;

*/


typedef struct{
    int Energy = -1;
    bool IsMassive = false;
    standardCube KindOfCube = none;
    bool IsGermForMountainOrValley = false;
    //!position PositionOfCube;
    int IDofAccountSittingAtThisCube = -1;
    //! vector <int> IDsOfAccountsWhichCanSeeThisCube;

}StructWithVariables;



class cube{

    private:
      //  int Position[2] = {-1, -1};            //{Y, X}
        //position Position;

        int Energy = 100;                 //something from 0 to 100; it breaks, when it's 0
        bool IsMassive = false;
        //!bool CanBlockSight = false;
        standardCube KindOfCube = none;
        bool IsGermForMountainOrValley = false;
        //!int IDofAccountSittingAtThisCube = -1;



    public:
        cube();                                       //constructor for any cubes
        cube( int In_Energy, bool In_IsFlat,/*! bool In_CanBlockSight,*/ standardCube In_KindOfCube/*!, int In_IDofAccountSittingAtThisCube*/ );
        cube(standardCube In_StandardCube);       //constructor for standard cubes
        ~cube();

        friend bool operator==( const cube& In_Cube1, const cube& In_Cube2 );
        //!friend bool operator< ( const vector <cube>& In_LeftHandSide, const vector <cube>& In_RightHandSide );       //this is needed to sort the stack-vector of cubes from the SightField by Position

        standardCube getTypeOfCube();

        void setTypeOfCube( standardCube In_NewTypeOfCube, int In_UserIDofCharacter = -1 );

        //position getPosition() const;
     //   void setCubeType( standardCube In_NewTypeOfCube );
        void setIsGermForMountainOrValley(bool In_IsGermForMointainOrValley);
        bool isGermForMountainOrValley();
        int getEnergy();
        bool isMassive();
        //int getNumberOfKindOfCube();
        string getStringOfCubeType();
        //!void addIDofAAccountWhoCanSeeThisCube(int In_AccountIDtoAdd, int* In_Counter);
        //!void removeIDOfAccountFromVector_IDsOfAccountsWhichCanSeeThisCube(int In_AccountIDtoRemove, int* In_Counter);
        //!void setIDofAccountSittingAtThisCube(int In_AccountID);
        //!int getIDofAccountSittingAtThisCube();
        //!vector <int> getIDsOfAccountsWhichCanSeeThisCube();
        StructWithVariables getVariables() const;
        //!bool canBlockSight();




        //! thats for generating the json-file. will be needed later for saving stuff
/*!
        template <class Archive>
        void serialize(Archive & archive){
            archive(  CEREAL_NVP(Energy), CEREAL_NVP(IsFlat), CEREAL_NVP(KindOfCube), CEREAL_NVP(PositionOfCube) ); // serialize things by passing them to the archive
        }
*/

};












#endif //CUBE_H_INCLUDED
