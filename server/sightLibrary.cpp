#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>           //needed for timestamp
#include <math.h>

#include "sightLibrary.h"




using namespace std;



sightLibrary::sightLibrary(float In_RatioZ_RadiusToSightRadius){

    RatioZ_RadiusToSightRadius = In_RatioZ_RadiusToSightRadius;

    position First;
    First.PosY = 0;
    First.PosX = 0;
    First.PosZ = 1;
    MovementDirections.push_back( First );

    position Second;
    Second.PosY = 0;
    Second.PosX = 0;
    Second.PosZ = -1;
    MovementDirections.push_back( Second );

    position Third;
    Third.PosY = -1;
    Third.PosX = 0;
    Third.PosZ = 0;
    MovementDirections.push_back( Third );

    position Fourth;
    Fourth.PosY = 1;
    Fourth.PosX = 0;
    Fourth.PosZ = 0;
    MovementDirections.push_back( Fourth );

    position Fivth;
    Fivth.PosY = 0;
    Fivth.PosX = -1;
    Fivth.PosZ = 0;
    MovementDirections.push_back( Fivth );

    position Sixth;
    Sixth.PosY = 0;
    Sixth.PosX = 1;
    Sixth.PosZ = 0;
    MovementDirections.push_back( Sixth );

}

sightLibrary::~sightLibrary(){

}


void sightLibrary::calculateSightFieldMapWithRadius( int In_Radius ){
    //! this function gets called sController()-constructor multiple times.
    //! it just calculates positions, which are in a sphere (with the radius = In_Radius and the character in the center)
    //! it organizes the positions in 'shells' -> the list is sorted by distance from center
    //! due to calculating-power-limitation (late, in running game-play), we cut of the top and bottom segment. how big that segment is, is specified by 'RatioZ_RadiusToSightRadius'.
    //! after everything is calculated, the list will be added to 'SightFieldMap' and this function calls two others:
    //! calculatePositionsToAddAfterMovement(In_Radius) and calculatePositionsToRemoveAfterMovement(In_Radius);

    cout << getTimestampInMiliseconds() << " entering sightLibrary::calculateSightFieldMapWithRadius(). radius: " << In_Radius << endl;

    int RadiusSquared = pow( In_Radius, 2 );

    vector < vector < position > > Temp_SightFieldMap;
//! ^ this vector represents the 'shells' of an sightField (have a look at sightLibrary.h)
//!          ^ and this the is a list with positions in that shell

    vector < position > Dummy;

    for( int I = 0; I < In_Radius + 1; I++ ){
            Temp_SightFieldMap.push_back( Dummy );
    }

    position AimingAt;
    int Start = -1;
    int End = 1;
    int AllInAllCounter = 0;
    int ShellNumber = 0;
    while( AimingAt.PosY <= In_Radius ){
        for( AimingAt.PosY = Start; AimingAt.PosY <= End; AimingAt.PosY++ ){
            for( AimingAt.PosX = Start; AimingAt.PosX <= End; AimingAt.PosX++ ){
                for( AimingAt.PosZ = Start; AimingAt.PosZ <= End; AimingAt.PosZ++ ){

                    //checking whether AimingAt-position is in a valid area, or is in the area, which will be cut of, because we don't use the whole sphere
                    bool IsInValidArea = true;
                    if( AimingAt.PosZ < -(RatioZ_RadiusToSightRadius*In_Radius) || AimingAt.PosZ > (RatioZ_RadiusToSightRadius*In_Radius) ){
                        IsInValidArea = false;
                    }

                    if( IsInValidArea ){
                        if( pow( ( AimingAt.PosZ), 2 ) + pow( (AimingAt.PosY ), 2  ) + pow( ( AimingAt.PosX), 2 ) <= RadiusSquared  ){  //until now, we looked at a (giant) cube. now checking whether position is in a sphere

                            //but most of the positions have already be added to a inner shell...
                            bool IsAlreadyInVector = false;
                            for( size_t Check_AlreadyInVector_ShellIndex = /*1*/ 0; Check_AlreadyInVector_ShellIndex < Temp_SightFieldMap.size(); Check_AlreadyInVector_ShellIndex++ ){  //going through all shells
                                for( size_t Check_AlreadyInVector_Inner = 0; Check_AlreadyInVector_Inner < Temp_SightFieldMap[Check_AlreadyInVector_ShellIndex].size(); Check_AlreadyInVector_Inner++ ){
                                    if( Temp_SightFieldMap[Check_AlreadyInVector_ShellIndex][Check_AlreadyInVector_Inner] == AimingAt ){
                                        IsAlreadyInVector = true;
                                    }
                                }
                            }

                            if( IsAlreadyInVector == false ){ //if the position is not already in map
                                if( (AimingAt.PosY == 0 && AimingAt.PosX == 0 && AimingAt.PosZ == 0) == false ){   //i guess its better to exclude the middle position...
                                    AllInAllCounter++;  //for debugging and general interest

                                    //int GreatestDimension_Y_X = max( abs(AimingAt.PosY) , abs(AimingAt.PosX) );
                                    //int GreatestDimension_Y_X_Z = max( abs(AimingAt.PosZ), GreatestDimension_Y_X );
                                    int GreatestDimension_Y_X_Z = max( abs(AimingAt.PosZ), (max(abs(AimingAt.PosY), abs(AimingAt.PosX))) ); //get the biggest number of Y, X, and Z

                                    //cout << "ShellNumber: " << GreatestDimension_Y_X_Z << "; inserting AimingAt: " << AimingAt << endl;
                                    Temp_SightFieldMap[GreatestDimension_Y_X_Z].push_back( AimingAt );
                                }
                            }
                        }
                    }else{
                        //cout << "position is not in valid area, because of cut-off segments of sphere. position: " << AimingAt << endl;
                    }
                }
            }
        }
        Start--;
        End++;
        ShellNumber++;
    }
    cout << "AllInAllCounter: " << AllInAllCounter << endl;
    SightFieldMap[ In_Radius ] = Temp_SightFieldMap;
    calculatePositionsToAddAfterMovement(In_Radius);
    //!calculatePositionsToRemoveAfterMovement(In_Radius);
}


string sightLibrary::getTimestampInMiliseconds(){

    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int Time_Ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;

    string Output = to_string(Time_Ms);
    Output.erase( 0, 4 );

    return Output;
}


vector < vector < position >>* sightLibrary::returnPointerToSightFieldBlueprint( int In_Radius ){

    return &( SightFieldMap[In_Radius] );

}


void sightLibrary::calculatePositionsToAddAfterMovement( int In_Radius ){

    //! this function is about calculating the List of Positions which come in sight after a movement. this is important, because
    //! we don't want to transmit the whole sight-sphere all the time after each movement but only the changes.
    //! we do this by first calculating all positions which can be seen (sorted from inner to outer positions)
    //! -> a sphere.
    //! then i calculate a second sphere with an offset of one (in 6 different directions). and then i compare the first sphere
    //! with the second one. the positions which are not in the first one are new and can be added to the
    //! vector 'PositionsToAddAfterMovement'

    //! it is important to understand, that both, the vectors PositionsToAddAfterMovement and PositionsToRemovedAfterMovement
    //! contain the positions that need to be added/removed to the absolute position of the character AFTER movement, means with it's
    //! new position

    cout << getTimestampInMiliseconds() << " entering sightLibrary::calculatePositionsToAddAfterMovement(): In_Radius: " << In_Radius << endl;

    position AimingAt;

    /*
    //!debug:
    if( In_Radius == 1 ){
        cout << "break" << endl;
    }
    */


    //! so first the sphere before the virtual movement:
    vector < position > FirstSqhare;                        //will contain the positions inside and on the surface of a sphere with radius 'In_Radius'
    int RadiusSquared = pow( In_Radius, 2 );
    int Start = -1;
    int End = 1;
    while( AimingAt.PosY <= In_Radius ){
        for( AimingAt.PosY = Start; AimingAt.PosY <= End; AimingAt.PosY++ ){
            for( AimingAt.PosX = Start; AimingAt.PosX <= End; AimingAt.PosX++ ){
                for( AimingAt.PosZ = Start; AimingAt.PosZ <= End; AimingAt.PosZ++ ){
                    //cout << "AimingAt.PosZ: " << AimingAt.PosZ << endl;

                    /*
                    //!debugging
                    if( In_Radius == 3 && AimingAt.PosY == 0 && AimingAt.PosX == 0 && AimingAt.PosZ == -3 ){
                        cout << "break" << endl;
                    }
                    */

                    if( pow(AimingAt.PosY, 2) + pow(AimingAt.PosX, 2) + pow(AimingAt.PosZ, 2) <= RadiusSquared ){

                        if(AimingAt.PosZ <= In_Radius*RatioZ_RadiusToSightRadius){

                            if( AimingAt.PosZ >= -(In_Radius*RatioZ_RadiusToSightRadius) ){



                                bool AlreadyIn = false;
                                for( size_t Index = 0; Index < FirstSqhare.size(); Index++){
                                    if( FirstSqhare[Index] == AimingAt ){
                                         AlreadyIn = true;
                                    }
                                }

                                if( AlreadyIn == false ){
                                    //cout << "push_back: " << AimingAt << endl;
                                    FirstSqhare.push_back(AimingAt);
                                }

                            }

                        }





                    }
                }
            }
        }
        Start--;
        End++;
    }

    /*!FirstSphere:
    example: In_Radius = 4
    ---------------
    -------.-------
    -----.....-----
    ----.......----
    ----.......----
    ---....@....---
    ----.......----
    ----.......----
    -----.....-----
    -------.-------
    ---------------
    */

    for( size_t OneOfSixDirections = 0; OneOfSixDirections < MovementDirections.size() /* that is = 6 */; OneOfSixDirections++ ){

        /*
        //!debugging:
        if( In_Radius == 3 && OneOfSixDirections == 1 ){
            //cout << "break" << endl;
        }
        */

        //!just for information. don't delete!
        //!MovementDirections[0] = 0,0,1     //up in z
        //!MovementDirections[1] = 0,0,-1    //down in z
        //!MovementDirections[2] = -1,0,0    //up
        //!MovementDirections[3] = 1,0,0     //down
        //!MovementDirections[4] = 0,-1,0    //left
        //!MovementDirections[5] = 0,1,0     //right

        position Temp_InvertedDirection = MovementDirections[OneOfSixDirections] * (-1);
        vector < position > Result; //will contain positions which are come in sight after a movement of MovementDirection[OneOfSixDirections]
        for( size_t IndexOfFirstSphere = 0; IndexOfFirstSphere < FirstSqhare.size(); IndexOfFirstSphere++ ){
            position VectorBetweenCharacterAndPositionInFirstSphere = FirstSqhare[IndexOfFirstSphere] - Temp_InvertedDirection;


            /*!
            example: In_Radius = 4, MovementDirection = -1, 0, 0 -> OneOfSixDirections == 2
            ---------------
            -------.-------
            -----.....-----
            ----.....P.----     -> P = some position from FirstSphere (which is changing in this for-loop)
            ----.......----
            ---....@....---     -> @ -> position after movement
            ----...O...----     -> O -> original position
            ----.......----
            -----.....-----
            -------.-------
            ---------------
            -> VectorBetweenCharacterAndPositionInFirstSphere = vector from O to P
            */





            //!debugging
            //cout << "( pow(VectorBetweenCharacterAndPositionInFirstSphere.PosY, 2) + pow(VectorBetweenCharacterAndPositionInFirstSphere.PosX, 2) + pow(VectorBetweenCharacterAndPositionInFirstSphere.PosZ, 2) ): " << ( pow(VectorBetweenCharacterAndPositionInFirstSphere.PosY, 2) + pow(VectorBetweenCharacterAndPositionInFirstSphere.PosX, 2) + pow(VectorBetweenCharacterAndPositionInFirstSphere.PosZ, 2) ) << endl;

            if( ( pow(VectorBetweenCharacterAndPositionInFirstSphere.PosY, 2) + pow(VectorBetweenCharacterAndPositionInFirstSphere.PosX, 2) + pow(VectorBetweenCharacterAndPositionInFirstSphere.PosZ, 2) ) <= RadiusSquared ){
                //all positions with this conditions are not new in sightfield
                //we just ignore them
            }else{
                //but all other positions are those, comming new to sight field

                //cout << "OneOfSixDirections: " << OneOfSixDirections << " ; pushing back " << FirstSqhare[IndexOfFirstSphere] << endl;
                Result.push_back( FirstSqhare[IndexOfFirstSphere] );


                /*!
                example: In_Radius = 4, MovementDirection = -1, 0, 0 -> OneOfSixDirections == 2
                ---------------
                -------N-------
                -----NN.NN-----     -> N -> these positions have been added to Result. they came new to the sightField
                ----N.....N----
                ----.......----
                ---N...@...N---     -> @ -> position after movement
                ----...O...----     -> O -> position before movement
                ----.......----
                -----.....-----
                -------.-------
                ---------------
                */


            }
        }

        /*
        //!debuggin:
        if( In_Radius == 3 ){
            cout << "break" << endl;
        }
        */


        //! now we have the problem, receivedRequestToMove had the bottom and the top of the sphere-segment is not included. that means,
        //! for the Move-Vectors 0;0;1 and 0;0;-1 we need to add the top and bottom.
        if( RatioZ_RadiusToSightRadius != 1.0 ){
            if( OneOfSixDirections == 1 ){  //that is 0;0;-1
                AimingAt.PosY = -1;
                AimingAt.PosX = -1;
                AimingAt.PosZ = -1;
                int Start = -1;
                int End = 1;
                while( AimingAt.PosY <= In_Radius ){
                    for( AimingAt.PosY = Start; AimingAt.PosY <= End+1; AimingAt.PosY++ ){
                        for( AimingAt.PosX = Start; AimingAt.PosX <= End+1; AimingAt.PosX++ ){
                            for( AimingAt.PosZ = Start; AimingAt.PosZ <= End+1; AimingAt.PosZ++ ){
                                //cout << "AimingAt.PosZ: " << AimingAt.PosZ << endl;
                                if( AimingAt.PosZ == -(In_Radius*RatioZ_RadiusToSightRadius) ){
                                    Result.push_back( AimingAt );
                                    /*
                                    //!debugging:
                                    if( AimingAt.PosY == 0 && AimingAt.PosX == 0 ){
                                        //cout << "break" << endl;
                                    }
                                    */

                                }
                            }
                        }
                    }
                    Start--;
                    End++;
                }
            }else if( OneOfSixDirections == 0 ){        //that is 0;0;1
                AimingAt.PosY = -1;
                AimingAt.PosX = -1;
                AimingAt.PosZ = -1;
                int Start = -1;
                int End = 1;
                while( AimingAt.PosY <= In_Radius ){
                    for( AimingAt.PosY = Start; AimingAt.PosY <= End+1; AimingAt.PosY++ ){
                        for( AimingAt.PosX = Start; AimingAt.PosX <= End+1; AimingAt.PosX++ ){
                            for( AimingAt.PosZ = Start; AimingAt.PosZ <= End+1; AimingAt.PosZ++ ){
                                if( AimingAt.PosZ == (In_Radius*RatioZ_RadiusToSightRadius) ){
                                    Result.push_back( AimingAt );
                                }
                            }
                        }
                    }
                    Start--;
                    End++;
                }
            }
        }
        position Temp_MoveVector = MovementDirections[ OneOfSixDirections ];
        PositionsToAddAfterMovement[ In_Radius ][ Temp_MoveVector.PosY ][ Temp_MoveVector.PosX ][ Temp_MoveVector.PosZ ] = Result;
    }

    cout << getTimestampInMiliseconds() << " leaving sightLibrary::calculatePositionsToAddAfterMovement()" << endl;
}



vector < position >* sightLibrary::getPointerToPositionsToBeAddedToSightField( int In_SightRadius, position In_MovementVector){

    return &(PositionsToAddAfterMovement[In_SightRadius][In_MovementVector.PosY][In_MovementVector.PosX][In_MovementVector.PosZ]);


}

/*
vector < position >* sightLibrary::getPointerToPositionsToBeRemovedFromSightField( int In_SightRadius, position In_MovementVector){

    return &(PositionsToRemovedAfterMovement[In_SightRadius][In_MovementVector.PosY][In_MovementVector.PosX][In_MovementVector.PosZ]);

}
*/
