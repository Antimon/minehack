#ifndef WEAPON_H_INCLUDED
#define WEAPON_H_INCLUDED

#include <string>

#include "item.h"


using namespace std;



class weapon: public item {

    public:

        weapon( string In_Name, float In_Range, int In_Level, int In_Hitpoints );
        float getRange();
        int getHitpoints();

    protected:
        int Hitpoints = 0;
        float Range;

};














#endif // WEAPON_H_INCLUDED
