#ifndef CRAFTING_H
#define CRAFTING_H

#include <string>

#include "action.h"
#include "craftingTool.h"

using namespace std;

class crafting : public action{

    public:
        crafting( string In_NameOfCraftingAction );
        ~crafting();
        //crafting(const crafting& other);
        crafting& operator=(const crafting& other);
        standardCube getWhatCanBeCrafted(){return WhatCanBeCrafted;};
        void setCraftingTool( craftingTool* In_ToUseTool );

    protected:
        standardCube WhatCanBeCrafted;
        craftingTool* UsedTool;

    private:

};

#endif // CRAFTING_H
